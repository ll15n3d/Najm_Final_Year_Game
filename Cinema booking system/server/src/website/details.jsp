<!doctype html>
<%@ page import="server.Main,server.database.*,java.text.SimpleDateFormat,java.util.*" %>
<%
String movieId = request.getParameter("movieId");
boolean redirect = false;
if (movieId != null && !redirect) {
  Movies movie = Main.database.getMovie(Integer.parseInt(movieId));
  if (movie != null && !redirect) {
%>

<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon" />

  <title>Home</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet"href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Voltaire|Roboto|Overpass" rel="stylesheet">
  <!-- CSS Files -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/muv.css" rel="stylesheet"/>




</head>


<body class="profile-page">

  <nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
      <!-- pand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a href="index.jsp"class="logo-container">
          <img src="images/logo.png" class="logo-container" id="logo-container">
        </a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>


      <div class="collapse navbar-collapse site-navigation-row" id="navigation">
        <ul class="nav navbar-nav navbar-right">
          <li>
              <a href="whatson.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">WHAT'S ON</h2>
            </a>
          </li>
          <li>
            <a href="offers.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">OFFERS</h2>
            </a>
          </li>

          <li>
            <a href="vip.jsp" class=" link is-notactive mdl-navigation__link">
              <h2 class="nav-title">VIP</h2>
            </a>
          </li>
          <li>
            <a href="info.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">INFORMATION</h2>
            </a>
          </li>
          <li class="nav-item">
              <a href="profile.jsp" class="nav-link profile">
                  <i class="material-icons profile-container2">account_circle</i>
                  <h6>Profile</h6>
              </a>
          </li>
          <% if (Main.login.checkLogin(request)) { %>
          <li class="nav-item">
              <a href="signOut.jsp" class="nav-link profile">
                  <i class="material-icons join-container2">event_seat</i>
                  <h6>Log Out</h6>
              </a>
          </li>
          <%} else { %>
          <li class="nav-item">
              <a href="Login.jsp" class="nav-link profile">
                  <i class="material-icons join-container2">event_seat</i>
                  <h6>sign in</h6>
              </a>
          </li>
          <% } %>
          <li>
            <form action="search.jsp" method="get">
              <input type="text" class="input-lg input search" name="search" placeholder="Search...">
            </form>
            </li>
        </ul>
      </div>
    </div>
  </nav>

    <a class="btn pulse cd-top" href="#0" title="Back to top" id="myBtn">
    <i class="material-icons">arrow_upward</i></a>
    <div class="wrapper">
  <% String banner = null;
  String poster = null;
  String trailer = null;
  String behind = null;
  String interview = null;
   List<Images> images = Main.database.getImages(movie.getMovieId());
    for (Images image : images) {
      String id = image.getImageId().substring(0, image.getImageId().length() - 4).toLowerCase();
      if (id.endsWith("detailsbanner")) {
        banner = image.getImageId();
      } else if (id.endsWith("poster")) {
        poster = image.getImageId();
      } else if (id.endsWith("trailer")) {
        trailer = image.getImageId();
      } else if (id.endsWith("behind")) {
        behind = image.getImageId();
      } else if (id.endsWith("interview")) {
        interview = image.getImageId();
      }
    }%>
      <div class="header header-filter"  style="background-image: url('images/<%= banner%>')" style="width:100%;">
        <a href="details.jsp?movieId=<%= movie.getMovieId() %>" target="_self" class="link-movie">
          <div class="banner-caption">
            <a href="<%= movie.getyoutubeLink()%>" class="link-movie" target="_blank">
                  <img src="images/play_48.png">
          </div>
        </div>
      </a>

        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="unpeakable-container">




              </div>

            </div>
          </div>
        </div>
      </div>

    <div class="main">
      <div class="container">
        <div class="section text-center section-index">
          <div class="row">
            <div class="col-md-6 col-md-offset-2">

            </div>
          </div>


                <div class="overview-meta features">

                    <div>
                          <div class="dashboard display-animation" style="margin: auto; width: auto;">
                            <img src="images/<%= poster%>" alt="Movie Poster" style="width:100%">
                            </div>


                        <hr>
                            <dl class="display-animation">
                                <dt>Release date</dt>
                                <% String releaseDate = new SimpleDateFormat("dd/MM/yyyy").format(movie.getreleaseDate().getTime()); %>
                                <dd><%= releaseDate %> </dd>
                            </dl>
                                <dl class="display-animation">
                                    <dt>Running time</dt>
                                    <% String runningTime = new SimpleDateFormat("H:mm").format(movie.getDuration().getTime()); %>
                                    <dd><%= runningTime %> hrs</dd>
                                </dl>
                            <dl class="display-animation">
                                <dt>Director</dt>
                                <dd><%= movie.getDirector()%></dd>
                            </dl>
                            <dl class="display-animation">
                                <dt>Cast</dt>
                                <dd><%= movie.getLead_actors().replaceAll("\n", "<br />")%></dd>
                                <%-- <dd>Michael B. Jordan</dd>
                                  <dd>Lupita Nyong'o</dd> --%>
                            </dl>
                        </div>
                    </div>




                <div class="container">
                  <h1 class="detail-title"><%= movie.getName() %></h1>

                  <!-- <p class="details"><img src="images/12a.png" alt="12A" style="width:3%"></p> -->
                  <h5><%= movie.getCertificate()%></h5>



                  <div>
                   <i class="material-icons<%= (movie.getRating() >= 0.0) ? " star" : "" %>">star</i>
                   <i class="material-icons<%= (movie.getRating() >= 2.0) ? " star" : "" %>">star</i>
                   <i class="material-icons<%= (movie.getRating() >= 4.0) ? " star" : "" %>">star</i>
                   <i class="material-icons<%= (movie.getRating() >= 6.0) ? " star" : "" %>">star</i>
                   <i class="material-icons<%= (movie.getRating() >= 8.0) ? " star" : "" %>">star</i>
                  </div>


                  <div><p class="p2">
                      <%= movie.getBlurb()%></p>
                  </div>

                  <!-- <button type="button" class="btn2  buttonG" data-toggle="collapse" data-target="#rm">Read the full synopsis</button>

                  <div id="rm" class="collapse">


                    <div><p class="p2">Fresh from helping Captain America in the Civil War battle, Prince T’Challa (Chadwick Boseman) returns home, where his father, The King of Wakanda (John Kani), has just passed. </p>
                    <p class="p2">No sooner has T’Challa taken on the role as the reclusive, yet futuristic country’s new leader, trouble begins to pew. </p>
                    <p class="p2">Two separate foes are plotting to destroy Wakanda, and have their sights firmly set on dethroning the new king in order to realise their goal. Assuming the role of his alias, Black Panther, T’Challa teams up with C.I.A Agent Everett K. Ross (Martin Freeman) and elite members of the Dora Milaje (Wakandan Special Forces) in order to save his people and prevent another world war.</p>
                    <p class="p2"><em>Black Panther</em> has been a long time coming for Marvel fans, who have waited decades for the character to appear in his own film. Directed by the acclaimed Ryan Kugler (<em>Creed, Fruitvale Station</em>), the film stars Chadwick Boseman (<em>Get on Up</em>,<em> Avengers: Infinity War</em>) as the lead, having signed a five picture deal with Marvel.</p>
                    <p class="p2">
                    The excellent supporting cast is led by Lupita Nyong’o (<em>Star Wars: The Last Jedi</em>) and also features Michael B. Jordan (<em>Creed II</em>) along with Martin Freeman (<em>The Hobbit: The Battle of the Five Armies</em>). </p>
                  </p>
                  </div> -->

                <%-- </div> --%>

                  <div>
                <div type="button" id="muv" href="#" class="btn button1 buttonG">  GET ALL TIMES & TICKETS  </div>
              </div>








<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <span class="cal-title"><%= movie.getName() %></span>
      <p class="details"><%= movie.getCertificate()%></p>

      <hr>
    </div>
    <div>
    <%
    String date = "";
    int i = 0;
    List<TimeSlots> slots = Main.database.eachMovieTimeSlots(Integer.parseInt(movieId));
    for (TimeSlots slot : slots) {
      String newDate = new SimpleDateFormat("dd/MM/yyyy").format(slot.getStartTime().getTime());
      if (!date.equals(newDate)) {
    %>
      </div>
      <% if (i == 2) { %>
        <button type="button" class="btn2 buttonG collapsed" data-toggle="collapse" data-target="#extend">SHOW ALL TIME & DATES</button>
        <div id="extend" class="collapse">
      <% } %>
      <div class="modal-body row display-animation">
        <dl class="date-title"><%= newDate %></dl>

      <% date = newDate;
      i++;
        } %>
      <% String time = new SimpleDateFormat("hh:mm a").format(slot.getStartTime().getTime()); %>
      <a href="pay.jsp?timeId=<%= slot.getTimeId() %>" class="col-sm-2 btn body-details2 buttonP"><%= time %></a>
    <%
      } %>
  </div>
  <% if (i >= 2) { %>
    </div>
  <% } %>




  </div>
</div>






  <div class="col-sm-8">


              <div class="float">
              <hr align="left">
              <h2 class="title">FEATURES & VIDEOS</h2>


              <div id="myCarouse2" class="carousel slide" data-ride="carouse2">

                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarouse2" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarouse2" data-slide-to="1"></li>
                    <li data-target="#myCarouse2" data-slide-to="2"></li>

                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner carousel-small" id="carousel-inner">

                    <div class="item active header-filter">
                      <img src="images/<%= trailer%>" alt="Black Panther" style="width:100%;">
                      <div class="carousel-caption d-none d-md-block">
                        <a href="<%= movie.getyoutubeLink()%>" class="link-movie" target="_blank">
                              <img src="images/play.png"><h5>TRAILER</h5> <h2><%= movie.getName() %></h2>
                      </div>
                    </div>
                  </a>

                  <div class="item header-filter">
                    <img src="images/<%= interview%>" alt="interview" style="width:100%;">
                    <div class="carousel-caption d-none d-md-block">
                      <a href="<%= movie.getyoutubeLink()%>" class="link-movie" target="_blank">
                            <img src="images/interview.png"><h5>INTERVIEW</h5> <h2><%= movie.getName() %></h2>
                    </div>
                  </div>
                </a>

                <div class="item header-filter">
                  <img src="images/<%= behind%>" alt="Behind-Scene" style="width:100%;">
                  <div class="carousel-caption d-none d-md-block">
                    <a href="<%= movie.getyoutubeLink()%>" class="link-movie" target="_blank">
                          <img src="images/behind.png"><h5>BEHIND THE SCENE</h5> <h2><%= movie.getName()%></h2>
                  </div>
                </div>
              </a>
                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarouse2" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>

                  <a class="right carousel-control" href="#myCarouse2" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>

</div>
</div>



</div>
          <footer class="panel-footer" role="contentinfo">


          <div>
              <section class="container">
                  <h2 class="title2">
                      Customer service

                      <a href="tel:0121 211 4620"></a>
                            <span class="footer-title">0121 211 4620</span>
                  </h2>
                  <p>Call will be charged at your local rate.</p>

                  <div class="title2">SOCIAL</div>
                  <ul>
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-youtube"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                  </ul>
                  <div class="title2">GET IN TOUCH</div>
                  <ul>
                    <li><a href="info.jsp" class="footer-title">FAQ</a></li>
                    <li><a href="info.jsp" class="footer-title">Contact us</a></li>
                    <li><a href="info.jsp" class="footer-title">Feedback</a></li>
                    </ul>
                    <div class="title2">LEGAL</div>
                    <ul>
                      <li><a href="info.jsp" class="footer-title">About us</a></li>
                      <li><a href="info.jsp" class="footer-title">Terms & Conditions</a></li>
                      <li><a href="info.jsp" class="footer-title">Privacy</a></li>
                      <li><a href="info.jsp" class="footer-title">Accessability</a></li>
                      </ul>
                              </section>

                              <div>
                              <section href="index.jsp"class="container card-footer">
                                <img src="images/logo.png" class="logo-footer container">
                              </section>
                            </div>



                              <hr class="side">




              <div>
                  <section class="container card-footer">
                      <p> &#169; MUV. All rights reserved. </p>
                  </section>
              </div>
            </footer>





                <!--   Core JS Files   -->
                <script src="assets/js/jquery.min.js" type="text/javascript"></script>
                <!-- <script src="assets/js/bootstrap.min.js" type="text/javascript"></script> -->
                <script src="assets/js/counter.min.js" type="text/javascript"></script>
                <script src="assets/js/headingsindex.min.js" type="text/javascript"></script>
                <script src="assets/js/material.min.js"></script>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                <script src="js/scrollreveal.min.js"></script>
                <script src="http://code.jquery.com/jquery.js"></script>
                <script src="assets/js/jquery.waypoints.waypoints.min.js" type="text/javascript"></script>

                <!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
                <script src="assets/js/material-kit.js" type="text/javascript"></script>
                <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
<%
    } else {
      redirect = true;
    }
  } else {
    redirect = true;
  }
  if (redirect)
    response.sendRedirect("/whatson.jsp");
%>
