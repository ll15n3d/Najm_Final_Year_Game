<!doctype html>
<%@ page import="server.Main,server.database.*,java.util.List" %>

<%
if (Main.connection.check(request, response, false)) {
List<Movies> movies = Main.database.queryMovies();
%>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon" />

  <title>Home</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet"href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Voltaire|Roboto|Overpass" rel="stylesheet">
    <!-- CSS Files -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/muv.css" rel="stylesheet"/>




</head>


<body class="index-page">

  <nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a href="index.jsp"class="logo-container">
          <img src="images/logo.png" class="logo-container" id="logo-container">
        </a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>


      <div class="collapse navbar-collapse site-navigation-row" id="navigation">

        <ul class="nav navbar-nav navbar-right">
          <li>
              <a href="whatson.jsp" class="link active mdl-navigation__link">
              <h2 class="nav-title">WHAT'S ON</h2>
            </a>
          </li>
          <li>
            <a href="offers.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">OFFERS</h2>
            </a>
          </li>

          <li>
            <a href="vip.jsp" class=" link is-notactive mdl-navigation__link">
              <h2 class="nav-title">VIP</h2>
            </a>
          </li>
          <li>
            <a href="info.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">INFORMATION</h2>
            </a>
          </li>
          <li class="nav-item">
              <a href="profile.jsp" class="nav-link profile">
                  <i class="material-icons profile-container">account_circle</i>
                  <h6>Profile</h6>
              </a>
          </li>
          <% if (Main.login.checkLogin(request)) { %>
          <li class="nav-item">
              <a href="signOut.jsp" class="nav-link profile">
                  <i class="material-icons join-container">event_seat</i>
                  <h6>Log Out</h6>
              </a>
          </li>
          <%} else { %>
          <li class="nav-item">
              <a href="Login.jsp" class="nav-link profile">
                  <i class="material-icons join-container">event_seat</i>
                  <h6>sign in</h6>
              </a>
          </li>
          <% } %>
          <li>
            <form action="search.jsp" method="get">
              <input type="text" class="input-lg input search" name="search" placeholder="Search...">
            </form>
            </li>
        </ul>
      </div>
    </div>
  </nav>



  <a class="btn pulse cd-top" href="#0" title="Back to top" id="myBtn">
  <i class="material-icons">arrow_upward</i></a>
  <div class="wrapper">


      <div class="header wrapper">

        <div id="myCarousel3" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel3" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel3" data-slide-to="1"></li>
              <li data-target="#myCarousel3" data-slide-to="2"></li>
              <li data-target="#myCarousel3" data-slide-to="3"></li>
            </ol>

            <div class="carousel-inner" id="carousel-inner">
            <!-- Wrapper for slides -->
              <% int k = 0;
               for (int i = 0; i < movies.size() && k < 4; i++) {
                String banner = null;
                List<Images> images = Main.database.getImages(movies.get(i).getMovieId());
                for (Images image : images) {
                  if (image.getImageId().substring(0, image.getImageId().length() - 4).toLowerCase().endsWith("bannertop"))
                    banner = image.getImageId();
                }
                if (banner != null) {
                  k++;
                %>
              <div class="item<%= (i == 0) ? " active" : "" %> header-filter">
                <img src="images/<%= banner %>" alt="<%= movies.get(i).getName() %>" style="width:100%;">
                <div class="carousel-caption d-none d-md-block">
                  <a href="<%= movies.get(i).getyoutubeLink() %>" class="link-movie" target="_blank">
                        <img src="images/play_48.png"> <h1 class="detail-title"><%= movies.get(i).getName() %></h1>
                        </a>
                      <a href="details.jsp?movieId=<%= movies.get(i).getMovieId() %>" type="button"  class="btn buttonR">  BOOK NOW  </a>

                </div>
              </div>
        <%}
      }%>



            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel3" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>

            <a class="right carousel-control" href="#myCarousel3" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
        </div>




      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="unbreakable-container">



            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="main">
      <div class="container">
        <div class="section text-center section-index">
          <div class="row">

            <div class="col-md-6 col-md-offset-2">

            </div>
          </div>
          <hr align="left">


            <div class="animation-element bounce-up cf">
                <div class="subject development">
                  <h2 class="title">ALL FILMS</h2>



                        <div class="subject development">

                        <div class="row">
                        <% for (int i = 0; i < movies.size(); i++) { %>
                          <% if (i % 4 == 0 && i != 0) { %>
                            </div>
                            <div class="row">
                          <% } %>
                          <div class="col-md-3">
                            <div class="img-thumbnail button1 buttonB">
                              <a href="details.jsp?movieId=<%= movies.get(i).getMovieId() %>" target="_self" class="link-movie">
                                <%
                                  String poster = null;
                                  List<Images> images = Main.database.getImages(movies.get(i).getMovieId());
                                  for (Images image : images) {
                                    if (image.getImageId().substring(0, image.getImageId().length() - 4).toLowerCase().endsWith("poster"))
                                      poster = image.getImageId();
                                  }
                                %>
                                <img src="images/<%= (poster == null) ? "logo.png" : poster %>" alt="<%= movies.get(i).getName() %>" style="width:100%">
                                <div>
                                 <h3><%= movies.get(i).getName().toUpperCase() %></h3>
                                 <i class="material-icons<%= (movies.get(i).getRating() >= 0.0) ? " star" : "" %>">star</i>
                                 <i class="material-icons<%= (movies.get(i).getRating() >= 2.0) ? " star" : "" %>">star</i>
                                 <i class="material-icons<%= (movies.get(i).getRating() >= 4.0) ? " star" : "" %>">star</i>
                                 <i class="material-icons<%= (movies.get(i).getRating() >= 6.0) ? " star" : "" %>">star</i>
                                 <i class="material-icons<%= (movies.get(i).getRating() >= 8.0) ? " star" : "" %>">star</i>
                                </div>
                              </a>
                              <% if (movies.get(i).getComingSoon()) { %>
                                <label class="coming-soon">Coming soon</label>
                              <% } %>
                            </div>
                          </div>
                        <% } %>
                      </div>

                    </div>
                  </div>







</div>

  <footer class="panel-footer" role="contentinfo">


  <div>
      <section class="container">
          <h2 class="title2">
              Customer service

              <a href="tel:0121 211 4620"></a>
                    <span class="footer-title">0121 211 4620</span>
          </h2>
          <p>Call will be charged at your local rate.</p>

          <div class="title2">SOCIAL</div>
          <ul>
            <li><a href="#" class="fa fa-facebook"></a></li>
            <li><a href="#" class="fa fa-twitter"></a></li>
            <li><a href="#" class="fa fa-youtube"></a></li>
            <li><a href="#" class="fa fa-instagram"></a></li>
          </ul>
          <div class="title2">GET IN TOUCH</div>
          <ul>
            <li><a href="info.jsp" class="footer-title">FAQ</a></li>
            <li><a href="info.jsp" class="footer-title">Contact us</a></li>
            <li><a href="info.jsp" class="footer-title">Feedback</a></li>
            </ul>
            <div class="title2">LEGAL</div>
            <ul>
              <li><a href="info.jsp" class="footer-title">About us</a></li>
              <li><a href="info.jsp" class="footer-title">Terms & Conditions</a></li>
              <li><a href="info.jsp" class="footer-title">Privacy</a></li>
              <li><a href="info.jsp" class="footer-title">Accessability</a></li>
              </ul>
                      </section>

                      <div>
                      <section href="index.jsp"class="container ">
                        <img src="images/logo.png" class="logo-footer container">
                      </section>
                    </div>



                      <hr class="side">




      <div>
          <section class="container ">
              <p> &#169; MUV. All rights reserved. </p>
          </section>
      </div>
    </footer>







        <!--   Core JS Files   -->
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        <!-- <script src="assets/js/bootstrap.min.js" type="text/javascript"></script> -->
        <script src="assets/js/counter.min.js" type="text/javascript"></script>
        <script src="assets/js/headingsindex.min.js" type="text/javascript"></script>
        <script src="assets/js/material.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="js/scrollreveal.min.js"></script>
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="assets/js/jquery.waypoints.waypoints.min.js" type="text/javascript"></script>

        <!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
        <script src="assets/js/material-kit.js" type="text/javascript"></script>
        <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

        <% } else {
        response.sendRedirect("/index.jsp");
        } %>
