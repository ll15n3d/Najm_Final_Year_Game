<!doctype html>
<%@ page import="server.Main" %>
<%
boolean redirect = Main.login.checkLogin(request);
if (Main.connection.check(request, response, false) && !redirect) {
  String errorMessage = "";
  if (request.getParameter("signInButton") != null) {
    errorMessage = Main.login.checkLoginForm(request);
    if (errorMessage == "") {
      redirect = true;
    }
  }
  String createAccountError = null;
  if (request.getParameter("createButton") != null) {
    createAccountError = Main.login.checkCreateAccount(request);
  }
%>

<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Log In</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
  <link href="https://fonts.googleapis.com/css?family=Voltaire|Roboto|Overpass" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.rawgit.com/tonystar/bootstrap-float-label/v4.0.2/bootstrap-float-label.min.css"/>
    <!-- CSS Files -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/muv.css" rel="stylesheet"/>
  <!-- <link href="assets/css/logo-container.css" rel="stylesheet"/> -->
  <link rel="stylesheet" href="assets/css/material-photo-gallery.css">




</head>


<body class="profile-page">

  <nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a href="index.jsp"class="logo-container">
          <img src="images/logo.png" class="logo-container" id="logo-container">
        </a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>


      <div class="collapse navbar-collapse site-navigation-row" id="navigation">

        <ul class="nav navbar-nav navbar-right">
          <li>
              <a href="whatson.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">WHAT'S ON</h2>
            </a>
          </li>
          <li>
            <a href="offers.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">OFFERS</h2>
            </a>
          </li>

          <li>
            <a href="vip.jsp" class=" link is-notactive mdl-navigation__link">
              <h2 class="nav-title">VIP</h2>
            </a>
          </li>
          <li>
            <a href="info.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">INFORMATION</h2>
            </a>
          </li>
          <li class="nav-item">
              <a href="profile.jsp" class="nav-link profile">
                  <i class="material-icons profile-container2">account_circle</i>
                  <h6>Profile</h6>
              </a>
          </li>
          <% if (Main.login.checkLogin(request)) { %>
          <li class="nav-item">
              <a href="signOut.jsp" class="nav-link profile">
                  <i class="material-icons join-container2">event_seat</i>
                  <h6>Log Out</h6>
              </a>
          </li>
          <%} else { %>
          <li class="nav-item">
              <a href="Login.jsp" class="nav-link profile">
                  <i class="material-icons join-container2">event_seat</i>
                  <h6>sign in</h6>
              </a>
          </li>
          <% } %>
          <li>
            <form action="search.jsp" method="get">
              <input type="text" class="input-lg input search" name="search" placeholder="Search...">
            </form>
            </li>
        </ul>
      </div>
    </div>
  </nav>

  <a class="btn pulse cd-top" href="#0" title="Back to top" id="myBtn">
  <i class="material-icons">arrow_upward</i></a>
  <div class="wrapper">
    <div class="offersHead">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="unbreakable-container">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


      <div class="main">
        <div class="formbg">
          <div class="container">
            <div class="section text-center section-index">
              <div class="row">
                <div class="col-md-8 col-md-offset-2 borderLogin">
                    <div class="animation-element bounce-up cf">
                        <div class="LoginTitle"> <h1> Sign In To Your MUV </br></h1></div>
                        <div class="subject development">
                          <form class="form-horizontal" action="" method="post">
                          <div class="form-group profile">
                            <div class="col-sm-10">
                              <input type="txt" class="loginanimation" id="usr" placeholder="" name="username" required autocomplete="off">
                              <label class="" for="usr"><span>Username</span></label>
                            </div>
                          </div>
                          <div class="form-group center profile">
                            <div class="col-sm-10">
                              <input type="password" class=" loginanimation" id="pwd" placeholder="" name="pwd" required autocomplete="off">
                              <label class="" for="pwd"><span>Password</span></label>
                            </div>
                          </div>
                          <div class="form-group center profile">
                            <div class="col-sm-offset-2 col-sm-12">
                              <button id="signInButton" name="signInButton" method="post" type="submit" class="btn btn-danger">Sign In</button>
                            </div>
                          </div>
                        </form>
                        <% if (!"".equals(errorMessage)) { %>
                       <span> <h3 class="errorText"><%= errorMessage %> </h3></span>
                       <% } %>
                      </div>
                      </div>
                      <div class="LoginTitle"> <h1>Join MUV!</br></h1></div>
                      <div class="animation-element bounce-up cf">
                      <div class="subject development">
                        <form action="" method="post">
                          <div class="form-row">
                            <div class="form-group  profile col-md-6">
                              <input type="txt" class="loginanimation" id="name" placeholder="" name="firstname" required autocomplete="off">
                                <label class="" for="firstname"><span>Firstname</span></label>
                              </div>
                              <div class="form-group  profile col-md-6">
                                <input type="txt" class="loginanimation" id="lastname" placeholder="" name="lastname" required autocomplete="off">
                                  <label class="" for="lastname"><span>Lastname</span></label>
                                </div>
                              </div>
                              <form>
                                <div class="form-row">
                                  <div class="form-group  profile col-md-6">
                                    <input type="txt" class="loginanimation" id="username" placeholder="" name="username" required autocomplete="off">
                                      <label class="" for="username"><span>Username</span></label>
                                    </div>
                                    <div class="form-group  profile col-md-6">
                                      <input type="txt" class="loginanimation" id="email" onkeyup="validateEmail()" placeholder="" name="email" required autocomplete="off">
                                        <label class="" for="email"><span id="submitEmailMessage" class="submitMessage">Email</span></label>
                                      </div>
                                    </div>
                                    <form>
                                      <div class="form-row">
                                        <div class="form-group  profile col-md-6">
                                          <input type="password" class="loginanimation" onkeyup="checkPass()" id="password" placeholder="" name="password" required autocomplete="off">
                                            <label class="" for="password"><span id="colorMe">Password</span></label>
                                          </div>
                                          <div class="form-group  profile col-md-6">
                                            <input type="password" class="loginanimation" onkeyup="checkPass()" id="confirmpassword" placeholder="" name="confirmpassword" required autocomplete="off">
                                              <label class="" for="confirmpassword"><span id="colorMe1">Confirm Password</span></label>
                                            </div>
                                             </div>
                                              <div class="form-group profile">
                                                <div class="col-sm-offset-1 col-sm-10">
                                                  <button id="createButton" name="createButton" onsubmit="CreatePassword(); return false;" type="submit" class="btn btn-danger">Create</button></br>
                                                  <h3><span id="submitMessage" class="submitMessage"><%= (createAccountError == null) ? "" : ((createAccountError == "") ? "Successfully created account. You may login." : createAccountError) %></span></h3>
                                                </div>
                                              </div>
                                            </form>
                                          </div>
                                          </div>
                                        </div>
                                      </div>



                <div class=" profile">
                <footer class="panel-footer" role="contentinfo">


                <div>
                    <section class="container">
                        <h2 class="title2">
                            Customer service

                            <a href="tel:0121 211 4620"></a>
                                  <span class="footer-title">0121 211 4620</span>
                        </h2>
                        <p>Call will be charged at your local rate.</p>

                        <div class="title2">SOCIAL</div>
                        <ul>
                          <li><a href="#" class="fa fa-facebook"></a></li>
                          <li><a href="#" class="fa fa-twitter"></a></li>
                          <li><a href="#" class="fa fa-youtube"></a></li>
                          <li><a href="#" class="fa fa-instagram"></a></li>
                        </ul>
                        <div class="title2">GET IN TOUCH</div>
                        <ul>
                          <li><a href="info.jsp" class="footer-title">FAQ</a></li>
                          <li><a href="info.jsp" class="footer-title">Contact us</a></li>
                          <li><a href="info.jsp" class="footer-title">Feedback</a></li>
                          </ul>
                          <div class="title2">LEGAL</div>
                          <ul>
                            <li><a href="info.jsp" class="footer-title">About us</a></li>
                            <li><a href="info.jsp" class="footer-title">Terms & Conditions</a></li>
                            <li><a href="info.jsp" class="footer-title">Privacy</a></li>
                            <li><a href="info.jsp" class="footer-title">Accessability</a></li>
                            </ul>
                                    </section>

                                    <div>
                                    <section href="index.jsp"class="container ">
                                      <img src="images/logo.png" class="logo-footer container">
                                    </section>
                                  </div>



                                    <hr class="side">




                    <div>
                        <section class="container ">
                            <p> &#169; MUV. All rights reserved. </p>
                        </section>
                    </div>
                  </footer>
                </div>





                      <!--   Core JS Files   -->
                      <script src="assets/js/jquery.min.js" type="text/javascript"></script>
                      <script src="assets/js/passwordTest.js" type="text/javascript"></script>
                      <!-- <script src="assets/js/bootstrap.min.js" type="text/javascript"></script> -->
                      <script src="assets/js/counter.min.js" type="text/javascript"></script>
                      <script src="assets/js/headingsindex.min.js" type="text/javascript"></script>
                      <script src="assets/js/material.min.js"></script>
                      <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                      <script src="js/scrollreveal.min.js"></script>
                      <script src="http://code.jquery.com/jquery.js"></script>
                      <script src="assets/js/jquery.waypoints.waypoints.min.js" type="text/javascript"></script>

                      <!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
                      <script src="assets/js/material-kit.js" type="text/javascript"></script>
                      <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
<% }
if (redirect)
response.sendRedirect("/profile.jsp"); %>
