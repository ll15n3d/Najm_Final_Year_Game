var emailValid = false;
var monthValid = false;

function Price()
{
 var adult = document.getElementById('numofadults').value;
 var children = document.getElementById('numofchildren').value;
 var seniors = document.getElementById('numofseniors').value;

 var message = document.getElementById('submitMessage');
 var adultPrice = 8.5;
 var childrenPrice = 5;
 var seniorsPrice = 7.5;
 var total = 0;
 if (adult == "") adult = "0";
 if (children == "") children = "0";
 if (seniors == "") seniors = "0";
 total = (parseInt(adult) * adultPrice) + (parseInt(children) * childrenPrice) + (parseInt(seniors) * seniorsPrice);

 var vipDiv= document.getElementById("vipSeats");
 var VipButton = document.getElementById("seatButton");
 var messageVip = document.getElementById('vipMessage');
 var vipPrice = 2;
 var totalVip = 0;

 totalVip = (parseInt(adult) * vipPrice) + (parseInt(children) * vipPrice) + (parseInt(seniors) * vipPrice);

 var actual = total + totalVip;
 var z = 0;
 var VipMessage = "";
 if (document.getElementById("vipSeating").checked){
   z = "Pay: £" + actual;
   VipMessage = "I Don't Want VIP";
 }else {
   z = "Pay: £" + total;
   VipMessage = "I Want VIP For £" + totalVip;
 }
 message.innerHTML = z;
 messageVip.innerHTML = VipMessage;
}


function cardShow() {
    var cardButton = document.getElementById("cardButton");
    var PayRow = document.getElementById("payRow");
    var cardBox = document.getElementById("cardnumber");
    var cvc = document.getElementById("cvc");
    var month = document.getElementById("expirymonth");
    var year = document.getElementById("expiryyear");
    cvc.value = "";
    month.value = "";
    year.value = "";

        cardButton.classList.add("collapse");
        PayRow.classList.add("in");
        cardBox.value = "";
        cardBox.onkeydown=function(){return true;};
}


function showSeating() {
    var x = document.getElementById("vipSeats");
    if (document.getElementById("vipSeating").checked) {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
    Price();
}

function validateCard(value) {

  var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
      var matches = v.match(/\d{4,16}/g);
      var match = matches && matches[0] || ''
      var parts = []

      for (i=0, len=match.length; i<len; i+=4) {
          parts.push(match.substring(i, i+4))
      }

      if (parts.length) {
          return parts.join(' ')
      } else {
          return value
      }

}

function checkCard() {
    var cardNumber = document.getElementById("cardnumber");
    cardNumber.value = validateCard(cardNumber.value);
}

function checkCardLength(event) {
  checkCard();
  var cardNumber = document.getElementById("cardnumber");
  if (cardNumber.value.length >= 19) {
    return false;
  }
  return true;
}

function checkCvcLength(event) {
  var cardNumber = document.getElementById("cvc");
  if (cardNumber.value.length >= 4) {
    return false;
  }
  return true;
}

function checkNumLength(event) {
  var num = event.target;
  if (num.value.length >= 2) {
    return false;
  }
  return true;
}

function checkMonthLength(event) {
  var cardNumber = document.getElementById("expirymonth");
  if (cardNumber.value.length >= 2) {
    return false;
  }
  return true;
}

function checkYearLength(event) {
  var cardNumber = document.getElementById("expiryyear");
  if (cardNumber.value.length >= 2) {
    return false;
  }
  return true;
}


function checkDigit(event) {
    var code = (event.which) ? event.which : event.keyCode;

    if ((code < 48 || code > 57) && (code > 31)) {
        return false;
    }

    return true;
}


function validateEmail()
{
  var email = document.getElementById('email');
  var message = document.getElementById('submitEmailMessage');
  if (/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(email.value)) {
    message.innerHTML = "Email Valid";
    emailValid = true;
  } else {
    message.innerHTML = "Email Invalid";
    emailValid = false;
  }
  checkButton();
}
validateEmail();


function validateMonth()
{
  var month = document.getElementById('expirymonth');
  var message = document.getElementById('submitMonthMessage');
  if (parseInt(month.value) > 12 || parseInt(month.value) < 1) {
    message.innerHTML = "Expiry Month Invalid";
    monthValid = false;
  } else {
    message.innerHTML = "Expiry Month";
    monthValid = true;
  }
  checkButton();
}
validateMonth();


function checkButton() {
  document.getElementById("payButton").disabled = !(emailValid && monthValid);
}
