<!doctype html>
<%@ page import="server.Main" %>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <link rel="shortcut icon" href="images/logo.png" type="image/x-icon" />

  <title>VIP</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
  <link href="https://fonts.googleapis.com/css?family=Voltaire|Roboto|Overpass" rel="stylesheet">
  <!-- CSS Files -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="assets/css/muv.css" rel="stylesheet"/>




</head>


<body class="index-page">

  <nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>


      <div class="collapse navbar-collapse site-navigation-row" id="navigation">
        <a href="index.jsp"class="logo-container">
          <img src="images/logo.png" class="logo-container">
        </a>
        <ul class="nav navbar-nav navbar-right">
          <li>
              <a href="whatson.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">WHAT'S ON</h2>
            </a>
          </li>
          <li>
            <a href="offers.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">OFFERS</h2>
            </a>
          </li>

          <li>
            <a href="vip.jsp" class=" link active mdl-navigation__link">
              <h2 class="nav-title">VIP</h2>
            </a>
          </li>
          <li>
            <a href="info.jsp" class="link is-notactive mdl-navigation__link">
              <h2 class="nav-title">INFORMATION</h2>
            </a>
          </li>
          <li class="nav-item">
              <a href="profile.jsp" class="nav-link profile">
                  <i class="material-icons profile-container">account_circle</i>
                  <h6>Profile</h6>
              </a>
          </li>
          <% if (Main.login.checkLogin(request)) { %>
          <li class="nav-item">
              <a href="signOut.jsp" class="nav-link profile">
                  <i class="material-icons join-container">event_seat</i>
                  <h6>Log Out</h6>
              </a>
          </li>
          <%} else { %>
          <li class="nav-item">
              <a href="Login.jsp" class="nav-link profile">
                  <i class="material-icons join-container">event_seat</i>
                  <h6>sign in</h6>
              </a>
          </li>
          <% } %>
          <li>
                  <input type="text" class="input-lg input search" name="search" placeholder="Search...">
              </a>
            </li>
        </ul>
      </div>
    </div>
  </nav>



    <a class="btn pulse cd-top" href="#0" title="Back to top" id="myBtn">
    <i class="material-icons">arrow_upward</i></a>
    <div class="wrapper">


        <div class="header wrapper">

          <div id="myCarousel" class="carousel slide" data-ride="carousel">


              <!-- Wrapper for slides -->
              <div class="carousel-inner">


                <div class="item active">
                  <img src="images/vip.png" alt="VIP" style="width:100%;">
                  <div class="carousel-caption d-none d-md-block">
                  </div>
                </div>

            </div>
          </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <div class="unbreakable-container">



                </div>
              </div>
            </div>
          </div>

          <div class="main">
            <div class="container">
              <div class="section text-center section-index">
                <div class="row">
                  <div class="col-md-6 col-md-offset-2">

                  </div>
                </div>
                <hr align="left">

            <div class="animation-element bounce-up cf">
                <div class="subject development">
                  <h2 class="title">VIP</h2>




                    <div class="row col-md-12">

                  <div class="promos">
                  <div class="promo">
                    <div class="deal">
                      <span class="title2">BRONZE</span>
                      <span>Basic membership</span>
                    </div>
                    <span class="price">&#163;8</span>
                    <ul class=" ul2 features">
                      <li class="li2">8% of snacks</li>
                      <li class="li2">Unlimited films</li>
                      <li class="li2">And more...</li>
                    </ul>
                    <a href="Login.jsp"><button class="buttonvip">SIGN UP</button></a>
                  </div>
                  <div class="promo scale">
                    <div class="deal">
                      <span class="title2">GOLD</span>
                      <span>Just call us a second home!</span>
                    </div>
                    <span class="price">&#163;15</span>
                    <ul class="ul2 features">
                      <li class="li2">15% off snacks</li>
                      <li class="li2">Unlimited films</li>
                      <li class="li2">VIP seating</li>
                    </ul>
                      <a href="Login.jsp"><button class="buttonvip">SIGN UP</button></a>
                  </div>
                  <div class="promo">
                    <div class="deal">
                      <span class="title2">SILVER</span>
                      <span>Love coming on the weekends</span>
                    </div>
                    <span class="price">&#163;12</span>
                    <ul class="ul2 features">
                      <li class="li2">12% off snacks</li>
                      <li class="li2">Unlimited films</li>
                      <li class="li2">And more...</li>
                    </ul>
                    <a href="Login.jsp"><button class="buttonvip">SIGN UP</button></a>
                  </div>
                  </div>
                  </div>
                </div>


<hr class="black">
<hr class="black">
<hr class="black">
<hr class="black">

<hr>
<div><h2>SIGN-UP AND ENJOY THE PERKS OF BEING A VIP WITH BENEFITS SUCH AS</h2></div>

<div class="container dashboard display-animation">
  <div class="row">
    <div class="col-md-4">
      <div class="img-thumbnail">
        <a href="" target="_self" class="link-movie">
          <img src="images/stock3.jpg" alt="Unlimited" style="width:100%">
            <h4>Unlimited Movies</h4>
          <div class="caption-vip">
            <p>Watch all the films you can handle at any MUV for just one monthly fee.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="img-thumbnail">
        <a href="" target="_self" class="link-movie">
          <img src="images/stock2.jpg" alt="Discount" style="width:100%">
            <h4>Discount On Snacks</h4>
          <div class="caption-vip">
            <p>Get 8%, 12% or 15% of all in-cinema food and drink depending on your membership type. With the discounts also applying to local food businesses</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="img-thumbnail">
        <a href="" target="_self" class="link-movie">
          <img src="images/stock1.jpg" alt="VIP Seating" style="width:100%">
            <h4>VIP Seating On Selected Movies</h4>
          <a class="caption-vip">
            <p>Book early and we can guarantee you a set of seats for when yo arrive to share amongs family and friends. </p>
          </a>
        </a>
      </div>
    </div>
  </div>
</div>

<hr>
<h2>SOME STATS</h2>
<div class="container dashboard display-animation">
<div class="features">
    <div class="row">
        <div class="col-md-4">
            <div class="info">
                <div class="icon icon-info">
                    <i class="material-icons">supervisor_account</i>
                </div>
                <h4 class="title2">USERS</h4>
                <p>We currently have a staggering 3 million users in MUV club. We have Affordable prices for all types of movie goers be sure to enquire.</p>
                  <h3 id="up" class='numscroller title2 numscroller-big-bottom' data-slno='1' data-min='0' data-max='300465748' data-delay='15' data-increment="500000"></h3>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info">
                <div class="icon icon-success">
                    <i class="material-icons">verified_user</i>
                </div>
                <h4 class="title2">MOVIES</h4>
                <p>VIP members have watched a sensational 8 million+ movies. I mean why wouldn't you it's unlimited, just remember to get sunlight. </p>
                  <h3 id="up" class='numscroller title2 numscroller-big-bottom' data-slno='1' data-min='0' data-max='800784328' data-delay='15' data-increment="1300000"></h3>
            </div>
        </div>
        <div class="col-md-4">
            <div class="info">
                <div class="icon icon-shop">
                    <i class="material-icons">shopping_cart</i>
                </div>
                <h4 class="title2">DISCOUNTS</h4>
                <p>This month users have saved a combined total of £30,000+ on snacks. Thanks to the discounts from the VIP club. </p>
                <h3 id="up" class='numscroller title2 numscroller-big-bottom' data-slno='1' data-min='0' data-max='30465' data-delay='15' data-increment="50"></h3>
            </div>
        </div>
    </div>
</div>
</div>

<hr class="black">


  <div class="col-md-12">
<a type="button" href="Login.jsp" class="btn button2 buttonG">WOW! SIGN ME UP!</a>
</div>

<hr class="black">






<div class="text-center">

  <footer class="panel-footer" role="contentinfo">


      <section class="container">
          <h2 class="title2">
              Customer service

              <a href="tel:0121 211 4620"></a>
                    <span class="footer-title">0121 211 4620</span>
          </h2>
          <p>Call will be charged at your local rate.</p>

          <div class="title2">SOCIAL</div>
                                      <ul>
                                        <li><a href="#" class="fa fa-facebook"></a></li>
                                        <li><a href="#" class="fa fa-twitter"></a></li>
                                        <li><a href="#" class="fa fa-youtube"></a></li>
                                        <li><a href="#" class="fa fa-instagram"></a></li>
                                      </ul>
                                      <div class="title2">GET IN TOUCH</div>
                                      <ul>
                                        <li><a href="#" class="footer-title">FAQ</a></li>
                                        <li><a href="#" class="footer-title">Contact us</a></li>
                                        <li><a href="#" class="footer-title">Feedback</a></li>
                                        </ul>
                                        <div class="title2">LEGAL</div>
                                        <ul>
                                          <li><a href="#" class="footer-title">About us</a></li>
                                          <li><a href="#" class="footer-title">Terms & Conditions</a></li>
                                          <li><a href="#" class="footer-title">Privacy</a></li>
                                          <li><a href="#" class="footer-title">Accessability</a></li>
                                          </ul>
                      </section>

                      <div>
                      <section href="index.jsp"class="container ">
                        <img src="images/logo.png" class="logo-footer container">
                      </section>
                    </div>



                      <hr class="side">




      <div>
          <section class="container ">
              <p> &#169; MUV. All rights reserved. </p>
          </section>
      </div>
    </div>
    </footer>






        <!--   Core JS Files   -->
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        <!-- <script src="assets/js/bootstrap.min.js" type="text/javascript"></script> -->
        <script src="assets/js/counter.min.js" type="text/javascript"></script>
        <script src="assets/js/headingsindex.min.js" type="text/javascript"></script>
        <script src="assets/js/material.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="js/scrollreveal.min.js"></script>
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="assets/js/jquery.waypoints.waypoints.min.js" type="text/javascript"></script>

        <!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
        <script src="assets/js/material-kit.js" type="text/javascript"></script>
        <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
