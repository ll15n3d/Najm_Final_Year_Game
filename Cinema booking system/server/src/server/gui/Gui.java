package server.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import javax.swing.table.DefaultTableModel;

import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.data.Column;
import org.sql2o.data.Row;
import org.sql2o.data.Table;

import server.Main;

/**
 * GUI to make demo easier
 */
public class Gui {

  private JFrame frmServerGui;
  private JTable table;
  private DefaultTableModel tableModel;
  private String[] tables = new String[] { "Customers", "Staff", "Movies",
                                           "Images", "TimeSlots", "Seats",
                                           "Tickets", "Customer_Movies" };
  private JComboBox<String> comboBox;

  private enum colType {
                        PRIMARY_KEY, AUTO_PRIMARY_KEY, TEXT, MULTI_LINE_TEXT,
                        DATE, TIME, IMAGE, BOOLEAN, DOUBLE, INTEGER, PASSWORD,
                        FOREIGN_KEY, DELETE
  }

  private List<colType> columnTypes = new ArrayList<>();

  SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
  SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
  private Font mainFont;

  byte[] currentImage = null;
  private Dimension screen;

  /**
   * Create the application.
   */
  public Gui() {
    initialise();
  }

  /**
   * Start the GUI as a thread
   */
  public static void start() {
    try {
      UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
    } catch (Throwable e) {
      System.out.println(e);
    }
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        Gui window = new Gui();
        window.frmServerGui.setVisible(true);
      }
    });
  }

  private void refresh() {
    tableModel.setColumnCount(0);
    tableModel.setRowCount(0);
    columnTypes.clear();

    List<String> primaryKeys = new ArrayList<>();
    List<String> foreignKeys = new ArrayList<>();

    int selectedTable = comboBox.getSelectedIndex();
    String sql = "SELECT * FROM " + tables[selectedTable];
    try (Connection connection = Main.database.data.db.open()) {
      Table movies = connection.createQuery(sql).executeAndFetchTable();

      DatabaseMetaData metaData = connection.getJdbcConnection().getMetaData();
      ResultSet primary = metaData.getPrimaryKeys(null, null, movies.getName());
      while (primary.next()) {
        primaryKeys.add(primary.getString("COLUMN_NAME"));
      }
      ResultSet foreign =
                        metaData.getImportedKeys(null, null, movies.getName());
      while (foreign.next()) {
        foreignKeys.add(foreign.getString("FKCOLUMN_NAME"));
      }

      List<Column> columns = movies.columns();
      for (Column column : columns) {
        if (primaryKeys.contains(column.getName())) {
          if (column.getName().equalsIgnoreCase("IMAGEID")) {
            columnTypes.add(colType.PRIMARY_KEY);
          } else {
            columnTypes.add(colType.AUTO_PRIMARY_KEY);
          }
        } else if (foreignKeys.contains(column.getName())) {
          columnTypes.add(colType.FOREIGN_KEY);
        } else if (selectedTable == 2
                   && (column.getName().equalsIgnoreCase("BLURB")
                       || column.getName().equalsIgnoreCase("LEAD_ACTORS"))) {
          columnTypes.add(colType.MULTI_LINE_TEXT);
        } else if (column.getType().equalsIgnoreCase("BLOB")) {
          if (column.getName().equalsIgnoreCase("IMAGE")) {
            columnTypes.add(colType.IMAGE);
          } else {
            columnTypes.add(colType.PASSWORD);
          }
        } else if (column.getType().equalsIgnoreCase("BOOLEAN")) {
          columnTypes.add(colType.BOOLEAN);
        } else if (column.getType().equalsIgnoreCase("INTEGER")) {
          columnTypes.add(colType.INTEGER);
        } else if (column.getType().equalsIgnoreCase("DOUBLE")) {
          columnTypes.add(colType.DOUBLE);
        } else if (column.getType().equalsIgnoreCase("TIMESTAMP")) {
          if (column.getName().equalsIgnoreCase("DURATION")) {
            columnTypes.add(colType.TIME);
          } else {
            columnTypes.add(colType.DATE);
          }
        } else {
          columnTypes.add(colType.TEXT);
        }

        tableModel.addColumn(column.getName());
      }
      columnTypes.add(colType.DELETE);
      tableModel.addColumn("DELETE");

      List<Row> rows = movies.rows();
      for (Row row : rows) {
        Object[] data = new Object[columnTypes.size()];
        for (int i = 0; i < columnTypes.size(); i++) {
          if (columnTypes.get(i).equals(colType.PASSWORD)) {
            data[i] = "HIDDEN";
          } else if (columnTypes.get(i).equals(colType.IMAGE)) {
            data[i] = "IMAGE";
          } else if (columnTypes.get(i).equals(colType.DATE)) {
            data[i] = dateFormat.format(row.getDate(i));
          } else if (columnTypes.get(i).equals(colType.TIME)) {
            data[i] = timeFormat.format(row.getDate(i));
          } else if (columnTypes.get(i).equals(colType.DELETE)) {
            data[i] = "Delete";
          } else {
            data[i] = row.getObject(i);
          }
        }

        tableModel.addRow(data);
      }
    } catch (SQLException e) {
      System.out.println(e);
    }
  }

  private JPanel getDialogPanel(int row, int column, boolean newRow) {
    int numToEdit = 1;
    if (newRow) {
      column = 0;
      row = 0;
      numToEdit = columnTypes.size();
    }

    JPanel panel = new JPanel();
    frmServerGui.getContentPane().add(panel);
    GridBagLayout gbl_panel = new GridBagLayout();
    gbl_panel.columnWidths = new int[] { 50, 0, 200, 50 };
    gbl_panel.rowHeights = new int[2 * numToEdit + 1];
    gbl_panel.rowHeights[0] = 50;
    panel.setLayout(gbl_panel);

    for (int i = 0; i < numToEdit; i++) {
      if (columnTypes.get(column + i).equals(colType.AUTO_PRIMARY_KEY)
          || tableModel.getColumnName(column + i).equalsIgnoreCase("SALT")
          || columnTypes.get(column + i).equals(colType.DELETE)) {
        gbl_panel.rowHeights[2 * i + 1] = gbl_panel.rowHeights[2 * i + 2] = 0;
        continue;
      }
      gbl_panel.rowHeights[2 * i + 1] = gbl_panel.rowHeights[2 * i + 2] = 50;
      JLabel lbl = new JLabel(tableModel.getColumnName(column + i) + ": ");
      lbl.setFont(mainFont);
      GridBagConstraints gbc_lbl = new GridBagConstraints();
      gbc_lbl.anchor = GridBagConstraints.EAST;
      gbc_lbl.insets = new Insets(0, 0, 5, 5);
      gbc_lbl.gridx = 1;
      gbc_lbl.gridy = 2 * i + 1;
      panel.add(lbl, gbc_lbl);

      GridBagConstraints gbc = new GridBagConstraints();
      gbc.insets = new Insets(0, 0, 5, 5);
      gbc.fill = GridBagConstraints.BOTH;
      gbc.gridx = 2;
      gbc.gridy = 2 * i + 1;

      Object value = "";
      String text = "";
      if (!newRow) {
        value = tableModel.getValueAt(row, column + i);
        if (value != null) {
          if (value.getClass().getSimpleName().equalsIgnoreCase("double")) {
            text = Double.toString((Double) value);
          } else if (value.getClass().getSimpleName()
                          .equalsIgnoreCase("integer")) {
            text = Integer.toString((Integer) value);
          } else if (value.getClass().getSimpleName()
                          .equalsIgnoreCase("string")) {
            text = (String) value;
          }
        }
      }
      switch (columnTypes.get(column + i)) {
        case BOOLEAN:
          JCheckBox checkBox = new JCheckBox();
          if (!newRow) {
            checkBox.setSelected((boolean) value);
          }
          gbc.anchor = GridBagConstraints.WEST;
          gbc.fill = GridBagConstraints.NONE;
          panel.add(checkBox, gbc);
          break;
        case FOREIGN_KEY:
          try (Connection connection = Main.database.data.db.open()) {
            int selectedTable = comboBox.getSelectedIndex();
            String tableName = tables[selectedTable].toUpperCase();
            DatabaseMetaData metaData = connection.getJdbcConnection()
                                                  .getMetaData();
            ResultSet foreign = metaData.getImportedKeys(null, null, tableName);
            while (foreign.next()) {
              if (foreign.getString("FKCOLUMN_NAME")
                         .equals(tableModel.getColumnName(column + i))) {
                String table = foreign.getString("PKTABLE_NAME");
                String columnName = foreign.getString("PKCOLUMN_NAME");
                String sql = "SELECT " + columnName + " FROM " + table;
                Table key = connection.createQuery(sql).executeAndFetchTable();
                List<Row> keyRows = key.rows();
                Object[] values = new Object[keyRows.size() + 1];
                values[0] = "null";
                for (int j = 0; j < keyRows.size(); j++) {
                  values[j + 1] = keyRows.get(j).getObject(0);
                }
                JComboBox<Object> comboBox = new JComboBox<>(values);
                comboBox.setFont(mainFont);
                comboBox.setSelectedItem(value == null ? "null" : value);
                panel.add(comboBox, gbc);
              }
            }
          } catch (SQLException e1) {
            System.out.println(e1);
          }
          break;
        case IMAGE:
          if (!newRow) {
            int pKeyPos = columnTypes.indexOf(colType.PRIMARY_KEY);
            String imageName = (String) tableModel.getValueAt(row, pKeyPos);
            currentImage = Main.database.getImage(imageName);
          }
          JPanel panel_1 = new JPanel();
          panel_1.setLayout(new GridLayout(0, 2, 0, 0));

          JButton btnViewImage = new JButton("View Image");
          btnViewImage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              try {
                ByteArrayInputStream b = new ByteArrayInputStream(currentImage);
                BufferedImage image = ImageIO.read(b);
                Image img = image.getScaledInstance(
                                                    Math.min(image.getWidth(),
                                                             screen.width / 2),
                                                    -1, Image.SCALE_SMOOTH);
                JLabel imageLabel = new JLabel(new ImageIcon(img));
                JOptionPane.showMessageDialog(panel, imageLabel, "View image",
                                              JOptionPane.INFORMATION_MESSAGE);
              } catch (IOException e1) {
                System.out.println(e1);
              }
            }
          });
          btnViewImage.setFont(mainFont);
          panel_1.add(btnViewImage);

          JButton btnNewImage = new JButton("New Image");
          btnNewImage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              JFileChooser file = new JFileChooser();
              int d = file.showOpenDialog(panel);
              if (d == 0) {
                try {
                  Path path = file.getSelectedFile().toPath();
                  currentImage = Files.readAllBytes(path);
                } catch (IOException e1) {
                  System.out.println(e1);
                }
              }
            }
          });
          btnNewImage.setFont(mainFont);
          panel_1.add(btnNewImage);
          panel.add(panel_1, gbc);
          break;
        case MULTI_LINE_TEXT:
          JScrollPane scrollPane = new JScrollPane();
          JTextArea txtArea = new JTextArea();
          txtArea.setFont(mainFont);
          if (!newRow) {
            txtArea.setText(text);
          }
          scrollPane.setViewportView(txtArea);
          panel.add(scrollPane, gbc);
          gbl_panel.rowHeights[2 * i + 1] = 200;
          break;
        case DATE:
        case DOUBLE:
        case INTEGER:
        case TEXT:
        case TIME:
        case PRIMARY_KEY:
          JTextField txt = new JTextField();
          txt.setFont(mainFont);
          if (!newRow) {
            txt.setText(text);
          }
          txt.setColumns(100);
          panel.add(txt, gbc);
          break;
        case PASSWORD:
          JPasswordField pwd = new JPasswordField(100);
          pwd.setFont(mainFont);
          panel.add(pwd, gbc);
          break;
        case AUTO_PRIMARY_KEY:
        case DELETE:
        default:
          break;
      }
    }

    return panel;
  }

  private void modifyDatabase(JPanel panel, int row, int column,
                              boolean newRow) {
    String tableName = tables[comboBox.getSelectedIndex()];
    if (newRow) {
      String sql = "INSERT INTO " + tableName + " (";
      List<String> columns = new ArrayList<>();
      List<Object> values = new ArrayList<>();
      JTextField txt;
      byte[] salt = null;
      int offset = 0;
      for (int i = 0; i < columnTypes.size(); i++) {
        if (columnTypes.get(i).equals(colType.AUTO_PRIMARY_KEY)
            || columnTypes.get(i).equals(colType.DELETE)) {
          continue;
        }
        Component c = null;
        if (!tableModel.getColumnName(i).equalsIgnoreCase("SALT")) {
          c = panel.getComponent(2 * (columns.size() - offset) + 1);
        }
        columns.add(tableModel.getColumnName(i));
        try {
          switch (columnTypes.get(i)) {
            case BOOLEAN:
              JCheckBox checkBox = (JCheckBox) c;
              values.add(checkBox.isSelected());
              break;
            case DATE:
              txt = (JTextField) c;
              Timestamp startDate =
                                  new Timestamp(dateFormat.parse(txt.getText())
                                                          .getTime());
              values.add(startDate);
              break;
            case DOUBLE:
              txt = (JTextField) c;
              values.add(Double.parseDouble(txt.getText()));
              break;
            case FOREIGN_KEY:
              JComboBox<?> cBox = (JComboBox<?>) c;
              if ("null".equals(cBox.getSelectedItem())) {
                values.add(null);
              } else {
                values.add(cBox.getSelectedItem());
              }
              break;
            case IMAGE:
              values.add(currentImage);
              break;
            case INTEGER:
              txt = (JTextField) c;
              values.add(Integer.parseInt(txt.getText()));
              break;
            case MULTI_LINE_TEXT:
              JViewport v = (JViewport) ((JScrollPane) c).getComponent(0);
              JTextArea txtArea = (JTextArea) v.getComponent(0);
              values.add(txtArea.getText());
              break;
            case PRIMARY_KEY:
            case TEXT:
              txt = (JTextField) c;
              if (txt.getText().equalsIgnoreCase("null")) {
                columns.remove(columns.size() - 1);
                offset--;
              } else {
                values.add(txt.getText());
              }
              break;
            case TIME:
              txt = (JTextField) c;
              Timestamp startTime =
                                  new Timestamp(timeFormat.parse(txt.getText())
                                                          .getTime());
              values.add(startTime);
              break;
            case PASSWORD:
              if (columns.contains("USERNAME")) {
                if (salt == null) salt = getRandomSalt();
                if (tableModel.getColumnName(i).equalsIgnoreCase("SALT")) {
                  values.add(salt);
                  offset++;
                } else {
                  JPasswordField pwd = (JPasswordField) c;
                  values.add(hashPassword(new String(pwd.getPassword()), salt));
                }
              } else {
                columns.remove(columns.size() - 1);
                if (!tableModel.getColumnName(i).equalsIgnoreCase("SALT")) {
                  offset--;
                }
              }
              break;
            case AUTO_PRIMARY_KEY:
            case DELETE:
            default:
              break;
          }
        } catch (ParseException e) {
          System.out.println(e);
        }
      }
      sql += String.join(", ", columns);
      sql += ") VALUES (";
      for (int i = 0; i < columns.size(); i++) {
        sql += ":param" + i + ", ";
      }
      sql = sql.substring(0, sql.length() - 2) + ")";
      try (Connection connection = Main.database.data.db.open()) {
        Query query = connection.createQuery(sql);
        for (int i = 0; i < values.size(); i++) {
          query.addParameter("param" + i, values.get(i));
        }
        query.executeUpdate();
      }
    } else {
      int pKeyPos = Math.max(columnTypes.indexOf(colType.AUTO_PRIMARY_KEY),
                             columnTypes.indexOf(colType.PRIMARY_KEY));
      Object pKey = tableModel.getValueAt(row, pKeyPos);
      String pKeyColumn = tableModel.getColumnName(pKeyPos);
      String columnName = tableModel.getColumnName(column);
      String sql = "Update " + tableName + " SET " + columnName
                   + " = :param WHERE " + pKeyColumn + " = :pKey";

      try (Connection connection = Main.database.data.db.open()) {
        Query query = connection.createQuery(sql);
        Component c = panel.getComponent(1);
        JTextField txt;
        switch (columnTypes.get(column)) {
          case BOOLEAN:
            JCheckBox checkBox = (JCheckBox) c;
            query.addParameter("param", checkBox.isSelected());
            break;
          case DATE:
            txt = (JTextField) c;
            Timestamp startDate = new Timestamp(dateFormat.parse(txt.getText())
                                                          .getTime());
            query.addParameter("param", startDate);
            break;
          case DOUBLE:
            txt = (JTextField) c;
            query.addParameter("param", Double.parseDouble(txt.getText()));
            break;
          case FOREIGN_KEY:
            JComboBox<?> cBox = (JComboBox<?>) c;
            if ("null".equals(cBox.getSelectedItem())) {
              query.addParameter("param", (Object) null);
            } else {
              query.addParameter("param", cBox.getSelectedItem());
            }
            break;
          case IMAGE:
            query.addParameter("param", currentImage);
            break;
          case INTEGER:
            txt = (JTextField) c;
            query.addParameter("param", Integer.parseInt(txt.getText()));
            break;
          case MULTI_LINE_TEXT:
            JViewport v = (JViewport) ((JScrollPane) c).getComponent(0);
            JTextArea txtArea = (JTextArea) v.getComponent(0);
            query.addParameter("param", txtArea.getText());
            break;
          case TEXT:
            txt = (JTextField) c;
            if (txt.getText().equalsIgnoreCase("null")) {
              query.addParameter("param", (Object) null);
            } else {
              query.addParameter("param", txt.getText());
            }
            break;
          case TIME:
            txt = (JTextField) c;
            Timestamp startTime = new Timestamp(timeFormat.parse(txt.getText())
                                                          .getTime());
            query.addParameter("param", startTime);
            break;
          case PASSWORD:
          case PRIMARY_KEY:
          case AUTO_PRIMARY_KEY:
          case DELETE:
          default:
            break;
        }
        query.addParameter("pKey", pKey).executeUpdate();
      } catch (ParseException e) {
        System.out.println(e);
      }
    }
  }

  private void deleteRow(int row) {
    int pKeyPos = Math.max(columnTypes.indexOf(colType.AUTO_PRIMARY_KEY),
                           columnTypes.indexOf(colType.PRIMARY_KEY));
    Object pKey = tableModel.getValueAt(row, pKeyPos);
    String pKColumn = tableModel.getColumnName(pKeyPos);
    String tableName = tables[comboBox.getSelectedIndex()];
    String sql = "DELETE FROM " + tableName + " WHERE " + pKColumn + " = :pKey";
    try (Connection connection = Main.database.data.db.open()) {
      connection.createQuery(sql).addParameter("pKey", pKey).executeUpdate();
    }
  }

  /**
   * Initialise the contents of the frame.
   */
  private void initialise() {
    screen = Toolkit.getDefaultToolkit().getScreenSize();

    frmServerGui = new JFrame();
    frmServerGui.setTitle("Server GUI");
    frmServerGui.setBounds(50, 50, screen.width - 100, screen.height - 200);
    frmServerGui.setMinimumSize(new Dimension(900, 600));
    frmServerGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frmServerGui.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));

    mainFont = new Font("Segoe UI", Font.PLAIN, screen.width / 150);
    UIManager.put("Button.font", new FontUIResource(mainFont));

    JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
    frmServerGui.getContentPane().add(tabbedPane);

    JLabel mainLabel = new JLabel("Main");
    mainLabel.setFont(mainFont);
    JPanel mainPanel = new JPanel();
    tabbedPane.addTab("Main", null, mainPanel, null);
    GridBagLayout gbl_mainPanel = new GridBagLayout();
    gbl_mainPanel.columnWidths = new int[] { 255, 443, 0, 0 };
    gbl_mainPanel.rowHeights = new int[] { 0, 78 };
    gbl_mainPanel.columnWeights = new double[] { 1.0, 0.0, 0.0,
                                                 Double.MIN_VALUE };
    gbl_mainPanel.rowWeights = new double[] { 0.0, 1.0 };
    mainPanel.setLayout(gbl_mainPanel);

    comboBox = new JComboBox<String>();
    comboBox.setFont(mainFont);
    comboBox.setModel(new DefaultComboBoxModel<String>(tables));
    comboBox.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        refresh();
      }
    });
    GridBagConstraints gbc_comboBox = new GridBagConstraints();
    gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
    gbc_comboBox.insets = new Insets(0, 0, 5, 5);
    gbc_comboBox.gridx = 0;
    gbc_comboBox.gridy = 0;
    mainPanel.add(comboBox, gbc_comboBox);

    JButton btnRefresh = new JButton("Refresh");
    btnRefresh.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        refresh();
      }
    });
    btnRefresh.setFont(mainFont);
    GridBagConstraints gbc_btnRefresh = new GridBagConstraints();
    gbc_btnRefresh.weightx = 0.5;
    gbc_btnRefresh.anchor = GridBagConstraints.WEST;
    gbc_btnRefresh.insets = new Insets(0, 0, 5, 5);
    gbc_btnRefresh.gridx = 1;
    gbc_btnRefresh.gridy = 0;
    mainPanel.add(btnRefresh, gbc_btnRefresh);

    JButton btnNewRow = new JButton("Add a new row");
    btnNewRow.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        JPanel panel = getDialogPanel(0, 0, true);
        int d = JOptionPane.showConfirmDialog(frmServerGui, panel,
                                              "Edit database",
                                              JOptionPane.OK_CANCEL_OPTION);
        if (d == 0) {
          modifyDatabase(panel, 0, 0, true);
        }
        refresh();
      }
    });
    btnNewRow.setFont(mainFont);
    GridBagConstraints gbc_btnNewRow = new GridBagConstraints();
    gbc_btnNewRow.insets = new Insets(0, 0, 5, 0);
    gbc_btnNewRow.gridx = 2;
    gbc_btnNewRow.gridy = 0;
    mainPanel.add(btnNewRow, gbc_btnNewRow);

    JScrollPane scrollPane_1 = new JScrollPane();
    GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
    gbc_scrollPane_1.gridwidth = 3;
    gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
    gbc_scrollPane_1.gridx = 0;
    gbc_scrollPane_1.gridy = 1;
    mainPanel.add(scrollPane_1, gbc_scrollPane_1);
    tableModel = new DefaultTableModel() {
      private static final long serialVersionUID = 1L;

      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };

    table = new JTable();
    table.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
          Point clickPoint = e.getPoint();
          int row = table.rowAtPoint(clickPoint);
          int column = table.columnAtPoint(clickPoint);
          if (row >= 0 && column >= 0) {
            if (columnTypes.get(column).equals(colType.DELETE)) {
              JLabel label = new JLabel("Are you sure you would like to"
                                        + " delete this row?");
              label.setFont(mainFont);
              int d = JOptionPane.showConfirmDialog(frmServerGui, label,
                                                    "Edit database",
                                                    JOptionPane.YES_NO_OPTION);
              if (d == 0) {
                deleteRow(row);
              }
              refresh();
            } else if (!columnTypes.get(column).equals(colType.AUTO_PRIMARY_KEY)
                       && !columnTypes.get(column).equals(colType.PRIMARY_KEY)
                       && !columnTypes.get(column).equals(colType.PASSWORD)) {
              JPanel panel = getDialogPanel(row, column, false);
              int d =
                    JOptionPane.showConfirmDialog(frmServerGui, panel,
                                                  "Edit database",
                                                  JOptionPane.OK_CANCEL_OPTION);
              if (d == 0) {
                modifyDatabase(panel, row, column, false);
              }
              refresh();
            }
          }
        }
      }
    });
    scrollPane_1.setViewportView(table);
    table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    table.setRowSelectionAllowed(false);
    table.setFont(mainFont);
    table.getTableHeader().setFont(mainFont);
    table.setFillsViewportHeight(true);
    table.setModel(tableModel);
    table.setRowHeight(40);
    tabbedPane.setTabComponentAt(0, mainLabel);

    JLabel consoleLabel = new JLabel("Console");
    consoleLabel.setFont(mainFont);
    JPanel consolePanel = new JPanel();
    tabbedPane.addTab("Console", null, consolePanel, null);
    consolePanel.setLayout(new GridLayout(1, 0, 0, 0));

    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    consolePanel.add(scrollPane);

    JTextArea console = new JTextArea();
    console.setEditable(false);
    console.setLineWrap(true);
    scrollPane.setViewportView(console);
    console.setFont(mainFont);
    console.setBackground(Color.BLACK);
    console.setForeground(Color.WHITE);
    tabbedPane.setTabComponentAt(1, consoleLabel);

    PrintStream printStream = new PrintStream(new RedirectOutput(console));
    System.setOut(printStream);
    System.setErr(printStream);

    refresh();
  }

  private byte[] hashPassword(String password, byte[] salt) {
    try {
      // Hash password using PBKDF2 algorithm
      char[] pass = password.toCharArray();
      SecretKeyFactory s = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
      PBEKeySpec spec = new PBEKeySpec(pass, salt, 4096, 256);
      return s.generateSecret(spec).getEncoded();
    } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
      System.err.println(e);
    }
    return null;
  }

  private byte[] getRandomSalt() {
    SecureRandom r = new SecureRandom();
    byte[] salt = new byte[32];
    r.nextBytes(salt);
    return salt;
  }
}

/**
 * Class with OutputStream which places all text into a JTextArea
 */
class RedirectOutput extends OutputStream {
  private JTextArea textArea;

  public RedirectOutput(JTextArea textArea) {
    this.textArea = textArea;
  }

  @Override
  public void write(int b) throws IOException {
    textArea.append(String.valueOf((char) b));
    textArea.setCaretPosition(textArea.getDocument().getLength());
  }
}
