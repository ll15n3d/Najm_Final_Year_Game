package server.authentication;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.http.HttpServletRequest;

import server.Main;

/**
 *
 */
public class UserLogin {

  /**
   * Checks submit form for logging in
   *
   * @param request Http request from servlet or jsp
   *
   * @return String error message which is blank if no error
   */
  public String checkLoginForm(HttpServletRequest request) {
    String errorMessage = "";

    // Login form has been submit
    if (request.getParameter("signInButton") != null) {
      String username = request.getParameter("username");
      String password = request.getParameter("pwd");
      if (username != null && password != null) {
        if (checkLogin(request)) {
          errorMessage = "Error: User already logged in.";
        } else {
          if (!login(request, username, password)) {
            errorMessage = "Error: Incorrect username or password.";
          }
        }
      } else {
        errorMessage = "Error: Please provide a valid username and password.";
      }
    }
    return errorMessage;
  }

  /**
   * Checks submit form for create account
   *
   * @param request Http request from servlet or jsp
   *
   * @return String error message which is blank if no error
   */
  public String checkCreateAccount(HttpServletRequest request) {
    String errorMessage = "";

    // Login form has been submit
    if (request.getParameter("createButton") != null) {
      String fName = request.getParameter("firstname");
      String lName = request.getParameter("lastname");
      String username = request.getParameter("username");
      String email = request.getParameter("email");
      String password = request.getParameter("password");
      String confirm = request.getParameter("confirmpassword");
      if (fName != null && lName != null && username != null && email != null
          && password != null && confirm != null) {
        if (checkLogin(request)) {
          errorMessage = "Error: Cant create account as user is logged in.";
        } else {
          if (password.equals(confirm)) {
            if (!createAccountForCustomer(fName, lName, email, username,
                                          password, confirm)) {
              errorMessage = "Error: Username already taken.";
            }
          } else {
            errorMessage = "Error: Password do not match.";
          }
        }
      } else {
        errorMessage = "Error: Invalid details received.";
      }
    }
    return errorMessage;
  }

  /**
   * Logs user into server
   *
   * @param request Http request from servlet or jsp
   * @param username Username given for the account
   * @param password Password given for the account
   *
   * @return True if logged in, False otherwise
   */
  private boolean login(HttpServletRequest request, String username,
                        String password) {
    byte[] salt = Main.database.getSalt(username);
    if (Main.database.customerExistsFromUserName(username)) {
      if (Main.database.checkCustomerLogin(username,
                                           hashPassword(password, salt))) {
        request.getSession().setAttribute("user", "customer");

        request.getSession().setAttribute("customer",
                                          Main.database.getCustomer(username));

        return true;
      }
    }

    if (Main.database.staffExistsFromUserName(username)) {
      if (Main.database.checkStaffLogin(username,
                                        hashPassword(password, salt))) {
        request.getSession().setAttribute("user", "staff");

        request.getSession().setAttribute("staff",
                                          Main.database.getStaff(username));

        return true;
      }
    }

    return false;
  }

  /**
   * Logs out of server
   *
   * @param request Http request from servlet or jsp
   */
  public void logout(HttpServletRequest request) {
    request.getSession().removeAttribute("user");
    request.getSession().removeAttribute("customer");
    request.getSession().removeAttribute("staff");
  }

  /**
   * Checks if user is logged into system
   *
   * @param request Http request from servlet or jsp
   *
   * @return True if user logged in
   */
  public boolean checkLogin(HttpServletRequest request) {
    return request.getSession().getAttribute("user") != null;
  }

  /**
   * Adds a new user to the database
   * 
   * @param name
   * @param surname
   * @param email
   * @param username Username given for the account
   * @param password Password given for the account
   * @param confirm
   *
   * @return True if account created, False otherwise
   */
  public boolean createAccountForCustomer(String name, String surname,
                                          String email, String username,
                                          String password, String confirm) {
    if (!password.equals(confirm)) return false;

    if (!Main.database.customerExistsFromUserName(username)
        && !Main.database.staffExistsFromUserName(username)) {
      // Get a random salt
      byte[] salt = getRandomSalt();

      // Get the hashed password
      byte[] pass = hashPassword(password, salt);

      Main.database.insertNewCustomer(name, surname, email, false, username,
                                      pass, salt);
      return true;
    }
    return false;
  }

  /**
   * @param name
   * @param surname
   * @param email
   * @return
   */
  public int createAccountForGuest(String name, String surname, String email) {
    // Add user into database
    return Main.database.insertNewCustomer(name, surname, email, true, null,
                                           null, null);
  }

  /**
   * Adds a new staff member to the database
   *
   * @param username Username given for the account
   * @param password Password given for the account
   * @param confirm
   *
   * @return True if account created, False otherwise
   */
  public boolean createAccountForStaff(String username, String password,
                                       String confirm) {
    if (!password.equals(confirm)) return false;

    if (!Main.database.customerExistsFromUserName(username)
        && !Main.database.staffExistsFromUserName(username)) {

      // Get a random salt
      byte[] salt = getRandomSalt();

      // Get the hashed password
      byte[] pass = hashPassword(password, salt);

      Main.database.data.insertNewStaff(username, pass, salt);
      return true;
    }
    return false;
  }

  /**
   * Hashes the given password using the given salt
   *
   * @param password Password given for the account
   * @param salt Salt to hash the password with
   *
   * @return Byte array of the hashed password
   */
  private byte[] hashPassword(String password, byte[] salt) {
    try {
      // Hash password using PBKDF2 algorithm
      char[] pass = password.toCharArray();
      SecretKeyFactory s = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
      PBEKeySpec spec = new PBEKeySpec(pass, salt, 4096, 256);
      return s.generateSecret(spec).getEncoded();
    } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
      System.err.println(e);
    }
    return null;
  }

  /**
   * Creates a secure random 32 byte salt
   *
   * @return Byte array of a random salt
   */
  private byte[] getRandomSalt() {
    SecureRandom r = new SecureRandom();
    byte[] salt = new byte[32];
    r.nextBytes(salt);
    return salt;
  }

}
