package server.database;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * This holds the details for each movie (name,blurb,certificate,director,lead_actors,releaseDate,youtubeLink,duration,rate)
 */
public class Movies {

  private int movieId;
  private String name;
  private String blurb;
  private String certificate;
  private String director;
  private String lead_actors;
  private Timestamp releaseDate;
  private String youtubeLink;
  private Timestamp duration;

  private double rating;
  private boolean comingSoon;
  private boolean offer;
  private boolean family;
  private boolean student;
  private boolean classic;

  /**
   * getter to get the movieId
   * @return movieId
   */
  public int getMovieId() {

    return movieId;
  }

  /**
   * getter to get the movie name
   * @return name
   */
  public String getName() {

    return name;
  }

  /**
   * getter to get the blurb
   * @return blurb
   */
  public String getBlurb() {

    return blurb;
  }

  /**
   * getter to get the certificate
   * @return certificate
   */
  public String getCertificate() {

    return certificate;
  }

  /**
   * getter to get the director
   * @return director
   */
  public String getDirector() {

    return director;
  }

  /**
   * getter to get the lead_actors
   * @return lead_actors
   */
  public String getLead_actors() {

    return lead_actors;
  }

  /**
   * getter to get the release date
   * @return release date
   */
  public Calendar getreleaseDate() {
    Calendar release = Calendar.getInstance();
    release.setTimeInMillis(releaseDate.getTime());
    return release;
  }

  /**
   * getter to get the youtube link
   * @return youtubelink
   */
  public String getyoutubeLink() {

    return youtubeLink;
  }

  /**
   * getter to get the movie duration
   * @return duration
   */
  public Calendar getDuration() {
    Calendar movieDuration = Calendar.getInstance();
    movieDuration.setTimeInMillis(duration.getTime());
    return movieDuration;
  }

  /**
   * getter to get the rating
   * @return rating
   */
  public double getRating() {

    return rating;
  }

  /**
   * getter to get boolean for comingSoon
   * @return comingsoon
   */
  public boolean getComingSoon() {

    return comingSoon;
  }

  /**
   *  getter to get boolean for isoffer
   * @return offer
   */
  public boolean isOffer() {

    return offer;
  }

  /**
   *  getter to get boolean for isfamily
   * @return family
   */
  public boolean isFamily() {

    return family;
  }

  /**
   * getter to get boolean for isStudent
   * @return student
   */
  public boolean isStudent() {

    return student;
  }

  /**
   * getter to get boolean for isClassic
   * @return classic
   */
  public boolean isClassic() {

    return classic;
  }

}
