package server.database;

/**
 * This holds the seats for each movie wether seats are taken or free for a
 * movie.
 */
public class Seats {
  private int seatsId;
  private boolean seatTakenFlag;
  private boolean vipSeatFlag;
  private int timeslot_seat;

  /**
   * getter for the seatsId
   * @return seatsId
   */
  public int getSeatsId() {

    return seatsId;
  }

  /**
   * getter for the boolean seatstakenflag
   * @return seatTakenFlag
   */
  public boolean getSeatTakenFlag() {

    return seatTakenFlag;
  }

  /**
   * getter for the timeslot_seat link between timeslot and seats tables
   * @return timeslot_seat
   */
  public int getTimeslot_seat() {

    return timeslot_seat;
  }

  /**
   * getter for the vipseatsflag
   * @return vipseatflag
   */
  public boolean getVipSeatsFlag() {

    return vipSeatFlag;
  }
}
