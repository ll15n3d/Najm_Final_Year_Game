package server.database;

/**
 * This class deals with the database table images for the each movie many images to a movie
 */
public class Images {

  private String imageId;
  private byte[] image;
  private int image_movie;

  /**
   * getter to get the imageId
   * @return imageId
   */
  public String getImageId() {

    return imageId;
  }

  /**
   * getter to get the image
   * @return image
   */
  public byte[] getImage() {

    return image;
  }

  /**
   * getter to get the image_Movie links the movies and images together
   * @return image_movie
   */
  public int getImage_movie() {

    return image_movie;
  }

}
