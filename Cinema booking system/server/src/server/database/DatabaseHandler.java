package server.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.sql2o.Connection;

import server.Main;

/**
 * This processes the database data with specific functions which query the data
 */
public class DatabaseHandler {
  /**
   *
   */
  public DatabaseData data = new DatabaseData();

  /**
   * Initialises the database , creates the Tables for the database
   */
  public void startDatabase() {
    List<String> tables = new ArrayList<>();
    try {
      Connection connection = data.db.open();
      java.sql.Connection jdbcConn = connection.getJdbcConnection();
      String[] type = { "TABLE" };
      ResultSet rs = jdbcConn.getMetaData().getTables(null, null, null, type);
      while (rs.next()) {
        tables.add(rs.getString("TABLE_NAME"));
      }
    } catch (SQLException e) {
      System.err.println(e);
    }

    String[] requiredTables = { "Customers", "Staff", "Movies", "Images",
                                "TimeSlots", "Seats", "Tickets",
                                "Customer_Movies" };

    for (String table : requiredTables) {
      if (!tables.contains(table.toUpperCase())) {
        System.out.println("Creating " + table + " Table");
        switch (table) {
          case "Customers":
            data.createCustomerTable();
            break;
          case "Staff":
            data.createStaffTable();
            break;
          case "Movies":
            data.createMoviesTable();
            break;
          case "Images":
            data.createImagesTable();
            break;
          case "TimeSlots":
            data.createTimeSlotsTable();
            break;
          case "Seats":
            data.createSeatsTable();
            break;
          case "Tickets":
            data.createTicketsTable();
            break;
          case "Customer_Movies":
            data.createCustomer_MoviesTable();
            break;
          default:
            System.out.println("Error creating table");
            break;
        }
      }
    }
  }

  /**
   * produces a list of all the movies in the database
   * 
   * @return
   */
  public List<Movies> queryMovies() {
    String sql = "SELECT * FROM MOVIES";
    // connect to the database
    try (Connection connection = data.db.open()) {
      // add all the movies to a list from the database using Movie class
      List<Movies> movies = connection.createQuery(sql)
                                      .executeAndFetch(Movies.class);
      // return list
      return movies;
    }
  }

  /**
   * Gets movie from the movie id
   *
   * @param movieId
   * @return
   */
  public Movies getMovie(int movieId) {
    String sql = "SELECT * FROM Movies WHERE movieId=:movieId";
    // connect to the database
    try (Connection connection = data.db.open()) {
      // add all the movies to a list from the database using Movie class
      List<Movies> movies = connection.createQuery(sql)
                                      .addParameter("movieId", movieId)
                                      .executeAndFetch(Movies.class);
      // returns the movie matching the Id
      if (movies.size() > 0) return movies.get(0);
    }
    return null;
  }

  /**
   * Gets ticket from ticket id
   *
   * @param ticketId
   * @return
   */
  public Tickets getTicket(int ticketId) {
    String sql = "SELECT * FROM Tickets WHERE ticketId=:ticketId";
    // connect to the database
    try (Connection connection = data.db.open()) {
      // add all the movies to a list from the database using Movie class
      List<Tickets> tickets = connection.createQuery(sql)
                                        .addParameter("ticketId", ticketId)
                                        .executeAndFetch(Tickets.class);
      // returns the movie matching the Id
      if (tickets.size() > 0) return tickets.get(0);
    }
    return null;
  }

  /**
   * calculates the number of tickets sold within the database
   * 
   * @return
   */
  public int ticketsSold() {
    String sql = "SELECT * FROM Tickets";
    // connect to the database
    try (Connection connection = data.db.open()) {
      // add all the tickets sold to a list and then able to count using size
      List<Tickets> tickets = connection.createQuery(sql)
                                        .executeAndFetch(Tickets.class);
      // print out the ticket size
      return tickets.size();
    }
  }

  /**
   * Search movie database for name
   *
   * @param movieName The search parameter
   * @return List of movies found
   */
  public List<Movies> searchMovie(String movieName) {
    String sql = "SELECT * FROM Movies WHERE LOWER(name) LIKE :name";
    // Connect to the database
    try (Connection connection = data.db.open()) {
      // Get the movies which contain the name
      List<Movies> movies = connection.createQuery(sql)
                                      .addParameter("name",
                                                    "%" + movieName.toLowerCase()
                                                            + "%")
                                      .executeAndFetch(Movies.class);
      // Returns the list of movies
      return movies;
    }
  }

  /**
   * Get the seats at a specific timeslot
   *
   * @param timeId
   * @return
   */
  public List<Seats> getSeats(int timeId) {
    try (Connection connection = data.db.open()) {
      // Obtain the seats from the timeslot id
      String sql = "SELECT * FROM Seats WHERE timeslot_seat=:timeId";
      List<Seats> seats = connection.createQuery(sql)
                                    .addParameter("timeId", timeId)
                                    .executeAndFetch(Seats.class);
      // return the seats for that timeslot
      return seats;
    }
  }

  /**
   * obtain the number of seats available for each movie
   *
   * @param start
   * @param movieId
   * @return size of list array
   */
  public int seatsRemaining(Calendar start, int movieId) {
    Timestamp startTime = new Timestamp(start.getTimeInMillis());
    // get the number of seats from query using startTime and MovieId
    try (Connection connection = data.db.open()) {
      // obtain the timeId from the movieId and the startTime
      String sql = "SELECT Seats.seatTakenFlag FROM TimeSlots,Seats "
                   + "WHERE TimeSlots.startTime =:startTime AND "
                   + "TimeSlots.timeId=Seats.timeslot_seat AND "
                   + "timeslot_movieId =:timeslot_movieId";
      List<TimeSlots> timeslots =
                                connection.createQuery(sql)
                                          .addParameter("startTime", startTime)
                                          .addParameter("timeslot_movieId",
                                                        movieId)
                                          .executeAndFetch(TimeSlots.class);
      // print out the ticket size
      return timeslots.size();
    }
  }

  /**
   * Get the movie from the ticket id
   *
   * @param ticketId
   * @return
   */
  public Movies getMovieFromTicket(int ticketId) {
    try (Connection connection = data.db.open()) {
      String query = "SELECT Movies.* FROM Tickets "
                     + "JOIN Seats ON Tickets.ticket_seatId=Seats.seatsId "
                     + "JOIN TimeSlots ON Seats.timeslot_seat=TimeSlots.timeId "
                     + "JOIN Movies ON TimeSlots.timeslot_movieId=Movies.movieId "
                     + "WHERE Tickets.ticketId=:ticketId";
      List<Movies> movies = connection.createQuery(query)
                                      .addParameter("ticketId", ticketId)
                                      .executeAndFetch(Movies.class);
      // print out the ticket size
      if (movies.size() == 1) {
        return movies.get(0);
      }
      return null;
    }
  }

  /**
   * Get the movie from the ticket id
   * 
   * @param seatId
   * @return
   */
  public Movies getMovieFromSeat(int seatId) {
    try (Connection connection = data.db.open()) {
      String query = "SELECT Movies.* FROM Seats "
                     + "JOIN TimeSlots ON Seats.timeslot_seat=TimeSlots.timeId "
                     + "JOIN Movies ON TimeSlots.timeslot_movieId=Movies.movieId "
                     + "WHERE Seats.seatsId=:seatsId";
      List<Movies> movies = connection.createQuery(query)
                                      .addParameter("seatsId", seatId)
                                      .executeAndFetch(Movies.class);
      // print out the ticket size
      if (movies.size() == 1) {
        return movies.get(0);
      }
      return null;
    }
  }

  /**
   * Get the movie from the seat Id
   * 
   * @param seatId
   * @return
   */
  public TimeSlots getStartTimeFromSeat(int seatId) {
    try (Connection connection = data.db.open()) {
      String query = "SELECT TimeSlots.* FROM Seats "
                     + "JOIN TimeSlots ON Seats.timeslot_seat=TimeSlots.timeId "
                     + "WHERE Seats.seatsId=:seatsId";
      List<TimeSlots> timeslsots = connection.createQuery(query)
                                             .addParameter("seatsId", seatId)
                                             .executeAndFetch(TimeSlots.class);
      // print out the timeslot
      if (timeslsots.size() == 1) {
        return timeslsots.get(0);
      }
      return null;
    }
  }

  /**
   * Get the movie from the ticket id
   *
   * @param ticketId
   * @return
   */
  public TimeSlots getSlotFromTicket(int ticketId) {
    try (Connection connection = data.db.open()) {
      String query = "SELECT TimeSlots.* FROM Tickets "
                     + "JOIN Seats ON Tickets.ticket_seatId=Seats.seatsId "
                     + "JOIN TimeSlots ON Seats.timeslot_seat=TimeSlots.timeId "
                     + "WHERE Tickets.ticketId=:ticketId";
      List<TimeSlots> slots = connection.createQuery(query)
                                        .addParameter("ticketId", ticketId)
                                        .executeAndFetch(TimeSlots.class);
      // print out the ticket size
      if (slots.size() == 1) {
        return slots.get(0);
      }
      return null;
    }
  }

  /**
   * Get the image as a byte array from the image name
   *
   * @param name this is the image name
   * @return byte array of the image
   */
  public byte[] getImage(String name) {
    // connect to the database
    try (Connection connection = data.db.open()) {
      String sql = "SELECT * FROM Images WHERE imageId = :name";
      // return the image from the database
      List<Images> image =
                         connection.createQuery(sql).addParameter("name", name)
                                   .executeAndFetch(Images.class);
      // print out all the results
      if (!image.isEmpty()) return image.get(0).getImage();
    }
    return null;
  }

  /**
   * Get all the images related to this movie id
   *
   * @param movieId id of the movie the images are required for
   * @return byte array of the image
   */
  public List<Images> getImages(int movieId) {
    // connect to the database
    try (Connection connection = data.db.open()) {
      String sql = "SELECT * FROM Images WHERE image_movie=:movieId";
      // return the image from the database
      List<Images> images = connection.createQuery(sql)
                                      .addParameter("movieId", movieId)
                                      .executeAndFetch(Images.class);
      // print out all the results
      return images;
    }
  }

  /**
   * This obtains the list of all the images
   *
   * @return
   */
  public List<Images> getAllImage() {
    // connect to the database
    try (Connection connection = data.db.open()) {
      String sql = "SELECT * FROM images";
      // return the image from the database
      List<Images> image = connection.createQuery(sql)
                                     .executeAndFetch(Images.class);
      // print out all the results
      return image;
    }
  }

  /**
   * obtain a list of movies on at a time
   *
   * @param start
   * @return
   */
  public List<Movies> specificTimeSlots(Calendar start) {
    Timestamp startTime = new Timestamp(start.getTimeInMillis());

    Connection connection = data.db.open();
    String query = "SELECT Movies.movieId, Movies.name, Movies.blurb, "
                   + "Movies.certificate, Movies.Director, Movies.Lead_Actors "
                   + "FROM Movies, TimeSlots WHERE "
                   + "TimeSlots.startTime=:startTime AND "
                   + "TimeSlots.TIMESLOT_MOVIEID = Movies.MOVIEID";
    List<Movies> m = connection.createQuery(query)
                               .addParameter("startTime", startTime)
                               .executeAndFetch(Movies.class);
    return m;
  }

  /**
   *
   * @param movieId
   * @return
   */
  public List<TimeSlots> eachMovieTimeSlots(int movieId) {
    Connection connection = data.db.open();
    String query = "SELECT * FROM TimeSlots WHERE timeSlot_movieId=:movieId";
    List<TimeSlots> t = connection.createQuery(query)
                                  .addParameter("movieId", movieId)
                                  .executeAndFetch(TimeSlots.class);
    return t;
  }

  /**
   * Checks if a movie is already booked in for this slot
   *
   * @param start The start time for the slot
   * @param screen The screen for the slot
   * @return True if a movie is already booked for this slot and False otherwise
   */
  public boolean timeSlotExists(Calendar start, int screen) {
    Timestamp startTime = new Timestamp(start.getTimeInMillis());

    Connection connection = data.db.open();
    String query = "SELECT * FROM TimeSlots WHERE startTime=:startTime "
                   + "AND screen=:screen";
    List<TimeSlots> t = connection.createQuery(query)
                                  .addParameter("startTime", startTime)
                                  .addParameter("screen", screen)
                                  .executeAndFetch(TimeSlots.class);
    return t.size() > 0;
  }

  /**
   * Get all the timeslots for the movie that are playing today
   *
   * @param movieId
   * @return
   */
  public List<TimeSlots> todaySlots(int movieId) {
    Calendar start = Calendar.getInstance();

    start.set(Calendar.HOUR_OF_DAY, 0);
    start.set(Calendar.MINUTE, 0);
    start.set(Calendar.SECOND, 0);
    Timestamp startTime = new Timestamp(start.getTimeInMillis());

    start.add(Calendar.DAY_OF_MONTH, 1);
    Timestamp endTime = new Timestamp(start.getTimeInMillis());

    Connection connection = data.db.open();
    String query = "SELECT * FROM TimeSlots WHERE timeSlot_movieId=:movieId "
                   + "AND startTime BETWEEN :startTime AND :endTime";
    List<TimeSlots> t = connection.createQuery(query)
                                  .addParameter("movieId", movieId)
                                  .addParameter("startTime", startTime)
                                  .addParameter("endTime", endTime)
                                  .executeAndFetch(TimeSlots.class);
    return t;
  }

  /**
   * Gets a timeslot from its id
   *
   * @param timeId
   * @return
   */
  public TimeSlots getTimeslot(int timeId) {
    Connection connection = data.db.open();
    String query = "SELECT * FROM TimeSlots WHERE timeId=:timeId";
    List<TimeSlots> t = connection.createQuery(query)
                                  .addParameter("timeId", timeId)
                                  .executeAndFetch(TimeSlots.class);

    if (t.size() == 1) return t.get(0);

    return null;
  }

  /**
   * Gets number of repeats left for movie with id
   *
   * @param movieId
   * @return
   */
  public int getRepeats(int movieId) {
    Calendar now = Calendar.getInstance();
    Timestamp currentTime = new Timestamp(now.getTimeInMillis());

    Connection connection = data.db.open();
    String query = "SELECT * FROM TimeSlots WHERE timeSlot_movieId=:movieId "
                   + "AND startTime > :currentTime";
    List<TimeSlots> t = connection.createQuery(query)
                                  .addParameter("movieId", movieId)
                                  .addParameter("currentTime", currentTime)
                                  .executeAndFetch(TimeSlots.class);

    return t.size();
  }

  /**
   * Gets tickets sold today for a specific movie
   *
   * @param movieId
   * @return
   */
  public int getSold(int movieId) {
    Calendar start = Calendar.getInstance();

    start.set(Calendar.HOUR_OF_DAY, 0);
    start.set(Calendar.MINUTE, 0);
    start.set(Calendar.SECOND, 0);
    Timestamp startTime = new Timestamp(start.getTimeInMillis());

    start.add(Calendar.DAY_OF_MONTH, 1);
    Timestamp endTime = new Timestamp(start.getTimeInMillis());

    Connection connection = data.db.open();
    String query = "SELECT Tickets.* FROM Tickets "
                   + "JOIN Seats ON Tickets.ticket_seatId=Seats.seatsId "
                   + "JOIN TimeSlots ON Seats.timeslot_seat=TimeSlots.timeId "
                   + "JOIN Movies ON TimeSlots.timeslot_movieId=Movies.movieId "
                   + "WHERE Movies.movieId=:movieId And "
                   + "Tickets.timestamp BETWEEN :startTime AND :endTime";
    List<Tickets> t = connection.createQuery(query)
                                .addParameter("movieId", movieId)
                                .addParameter("startTime", startTime)
                                .addParameter("endTime", endTime)
                                .executeAndFetch(Tickets.class);

    return t.size();
  }

  /**
   * This lists all the staff members
   *
   * @return list of all staff members
   */
  public List<Staff> staffMembers() {
    try (Connection connection = data.db.open()) {
      String sql = "SELECT username FROM STAFF";
      // return the image from the database
      List<Staff> staff = connection.createQuery(sql)
                                    .executeAndFetch(Staff.class);
      return staff;
    }
  }

  /**
   * This adds a new customer into the database
   *
   * @param name users Name
   * @param surname users surName
   * @param guestFlag whether they are guests or not which
   * @param email email to email receipt
   * @param username login in details if want to login and save data
   * @param password login in details if want to login and save data
   * @param salt
   * @return
   */
  public int insertNewCustomer(String name, String surname, String email,
                               boolean guestFlag, String username,
                               byte[] password, byte[] salt) {
    int customerId = -1;
    try (Connection connection = data.db.beginTransaction()) {
      if (guestFlag == false) {
        String firstRecord = "INSERT INTO Customers (name, surname, "
                             + "email, guestFlag, username, password, salt) "
                             + "VALUES (:name, :surname, :email, :guestFlag, "
                             + ":username, :password, :salt)";
        customerId = connection.createQuery(firstRecord)
                               .addParameter("name", name)
                               .addParameter("surname", surname)
                               .addParameter("email", email)
                               .addParameter("guestFlag", guestFlag)
                               .addParameter("username", username)
                               .addParameter("password", password)
                               .addParameter("salt", salt).executeUpdate()
                               .getKey(Integer.class);
        connection.commit();
      } else if (guestFlag == true) {
        String firstRecord = "INSERT INTO Customers (name, surname, "
                             + "email, guestFlag) VALUES (:name, :surname, "
                             + ":email, :guestFlag)";
        customerId = connection.createQuery(firstRecord)
                               .addParameter("name", name)
                               .addParameter("surname", surname)
                               .addParameter("email", email)
                               .addParameter("guestFlag", guestFlag)
                               .executeUpdate().getKey(Integer.class);

        connection.commit();
      }
    }
    return customerId;
  }

  /**
   *
   * @param username
   * @param cardNumber
   * @return
   */
  public boolean updateCustomerCardDetails(String username, String cardNumber) {
    Connection connection = data.db.beginTransaction();
    String query = "Update Customers set cardNumber = :cardNumber WHERE "
                   + "LOWER(username)=:username";
    connection.createQuery(query).addParameter("cardNumber", cardNumber)
              .addParameter("username", username.toLowerCase()).executeUpdate()
              .getKey(Integer.class);
    connection.commit();

    return true;
  }

  /**
   * Checks submit form for purchasing tickets
   *
   * @param request Http request from servlet or jsp
   *
   * @return String error message which is blank if no error
   */
  public String checkTicketForm(HttpServletRequest request) {
    // Login form has been submit
    if (request.getParameter("payButton") != null) {
      Staff staff = null;

      String name = request.getParameter("name");
      String fName = "";
      String sName = "";
      if (name.contains(" ")) {
        fName = name.substring(0, name.indexOf(" "));
        sName = name.substring(fName.length() + 1);
      } else {
        fName = name;
      }
      String email = request.getParameter("email");
      String children = request.getParameter("numofchildren");
      String adults = request.getParameter("numofadults");
      String seniors = request.getParameter("numofseniors");
      String timeId = request.getParameter("timeid");
      String num = "\\d*", price = "\\d*(\\.\\d{1,2})?";
      if (name != null && email != null && children != null && adults != null
          && seniors != null && timeId != null && children.matches(num)
          && adults.matches(num) && seniors.matches(num)
          && timeId.matches(num)) {
        if (children.equals("")) children = "0";
        if (adults.equals("")) adults = "0";
        if (seniors.equals("")) seniors = "0";

        int c = Integer.parseInt(children);
        int a = Integer.parseInt(adults);
        int s = Integer.parseInt(seniors);
        int totalTickets = c + a + s;

        if (totalTickets == 0) {
          return "Error: No tickets booked.";
        }

        if (timeId.equals("")) timeId = "0";
        int timeslot = Integer.parseInt(timeId);

        TimeSlots slot = getTimeslot(timeslot);
        if (slot != null) {
          String account = (String) request.getSession().getAttribute("user");
          boolean guest = !"customer".equalsIgnoreCase(account);
          Customers cust = (Customers) request.getSession()
                                              .getAttribute("customer");
          String username = cust == null ? null : cust.getUsername();
          boolean isCashPayment = false;
          String staffId = null;
          if ("staff".equalsIgnoreCase(account)) {
            staff = (Staff) request.getSession().getAttribute("staff");
            staffId = staff.getStaffId() + "";
            String type = request.getParameter("type");
            isCashPayment = type.equalsIgnoreCase("cash");
          }

          String vip = request.getParameter("vipSeating");
          boolean vipSeat = "on".equalsIgnoreCase(vip);
          String[] vipSeats = request.getParameterValues("seat");

          if (!vipSeat || vipSeats.length == totalTickets) {
            List<Integer> seats = new ArrayList<>();
            if (vipSeat) {
              for (String i : vipSeats) {
                if (i.matches("\\d+")) {
                  if (seats.contains(i)) {
                    return "Error: Duplicate seat.";
                  } else {
                    int x = Integer.parseInt(i);
                    if (x > 20 || x < 1) {
                      return "Error: Invalid seat number.";
                    } else {
                      seats.add(x);
                    }
                  }
                } else {
                  return "Error: Invalid seat.";
                }
              }
            }
            double v = vipSeat ? 2 : 0;

            double cCost = 5 + v;
            double aCost = 8.5 + v;
            double sCost = 7.5 + v;
            double totalCost = c * cCost + a * aCost + s * sCost;

            List<Tickets> tickets = new ArrayList<>();
            String bookedSeats = "";
            if (isCashPayment) {
              String cost = request.getParameter("cost");
              String cash = request.getParameter("cash");
              if (cost != null && cash != null && cost.matches(price)
                  && cash.matches(price)) {
                double dCost = Double.parseDouble(cost);
                double dCash = Double.parseDouble(cash);
                if (dCash >= dCost) {
                  if (dCost == totalCost) {
                    double dChange = dCash - dCost;
                    if (vipSeat) {
                      List<Seats> availableSeats = getSeats(timeslot);
                      for (int i = availableSeats.size() - 1; i >= 0; i--) {
                        if (!availableSeats.get(i).getVipSeatsFlag()) {
                          availableSeats.remove(i);
                        }
                      }

                      for (int i = 0; i < seats.size(); i++) {
                        if (availableSeats.get(seats.get(i) - 1)
                                          .getSeatTakenFlag()) {
                          return "Error: Vip seat is already taken";
                        }
                      }

                      for (int i = 0; i < seats.size(); i++) {
                        String type = "";
                        if (c > 0) {
                          type = "Child";
                          c--;
                        } else if (a > 0) {
                          type = "Adult";
                          a--;
                        } else if (s > 0) {
                          type = "Senior";
                          s--;
                        }

                        bookedSeats += ",";
                        bookedSeats += (seats.get(i) < 11) ? "E" : "F";
                        bookedSeats +=
                                    ((seats.get(i) % 10) == 0) ? 10
                                                               : ((seats.get(i)
                                                                   % 10) == 0);
                        int ticket = addTicket(fName, sName, email, username,
                                               guest, type, "cash",
                                               availableSeats.get(seats.get(i)
                                                                  - 1),
                                               staffId);

                        tickets.add(getTicket(ticket));
                      }
                      bookedSeats = bookedSeats.substring(1);
                    } else {
                      List<Seats> availableSeats = getSeats(timeslot);
                      for (int i = availableSeats.size() - 1; i >= 0; i--) {
                        if (availableSeats.get(i).getSeatTakenFlag()
                            || availableSeats.get(i).getVipSeatsFlag()) {
                          availableSeats.remove(i);
                        }
                      }
                      if (availableSeats.size() < totalTickets) {
                        return "Error: Not enough seats available";
                      }
                      for (int i = 0; i < totalTickets; i++) {
                        String type = "";
                        if (c > 0) {
                          type = "Child";
                          c--;
                        } else if (a > 0) {
                          type = "Adult";
                          a--;
                        } else if (s > 0) {
                          type = "Senior";
                          s--;
                        }
                        int ticket = addTicket(fName, sName, email, username,
                                               guest, type, "cash",
                                               availableSeats.get(i), staffId);
                        tickets.add(getTicket(ticket));
                      }
                    }
                    return PdfQrCode.recieptTillCash(dCash, dCost, dChange, c,
                                                     cCost * c, a, aCost * a, s,
                                                     sCost * s, null, tickets,
                                                     vipSeat);
                  } else {
                    return "Error: Cost is incorrect.";
                  }
                } else {
                  return "Error: Cant pay less than cost of tickets.";
                }
              } else {
                return "Error: Please provide valid details.";
              }
            } else {
              String card = request.getParameter("cardnumber");
              if (!guest && card.contains("*")) card = cust.getRealCardNumber();
              String cvc = request.getParameter("cvc");
              String expiryMonth = request.getParameter("expirymonth");
              String expiryYear = request.getParameter("expiryyear");
              card = card.replaceAll("\\s+", "");
              if (card == null || cvc == null || expiryMonth == null
                  || expiryYear == null || card.length() != 16
                  || cvc.length() > 4 || cvc.length() < 3
                  || expiryMonth.length() > 2 || expiryMonth.length() < 1
                  || expiryYear.length() > 2 || expiryYear.length() < 1
                  || !card.matches(num) || !cvc.matches(num)
                  || !expiryMonth.matches(num) || !expiryYear.matches(num)
                  || Integer.parseInt(expiryMonth) > 12
                  || Integer.parseInt(expiryMonth) <= 0) {
                return "Error: Invalid card details";
              }
              if (vipSeat) {
                List<Seats> availableSeats = getSeats(timeslot);
                for (int i = availableSeats.size() - 1; i >= 0; i--) {
                  if (!availableSeats.get(i).getVipSeatsFlag()) {
                    availableSeats.remove(i);
                  }
                }

                for (int i = 0; i < seats.size(); i++) {
                  if (availableSeats.get(seats.get(i) - 1).getSeatTakenFlag()) {
                    return "Error: Vip seat is already taken";
                  }
                }

                for (int i = 0; i < seats.size(); i++) {
                  String type = "";
                  if (c > 0) {
                    type = "Child";
                    c--;
                  } else if (a > 0) {
                    type = "Adult";
                    a--;
                  } else if (s > 0) {
                    type = "Senior";
                    s--;
                  }

                  bookedSeats += ",";
                  bookedSeats += (seats.get(i) < 11) ? "E" : "F";
                  bookedSeats +=
                              ((seats.get(i) % 10) == 0) ? 10
                                                         : (seats.get(i) % 10);

                  int ticket = addTicket(fName, sName, email, username, guest,
                                         type, "card",
                                         availableSeats.get(seats.get(i) - 1),
                                         staffId);

                  tickets.add(getTicket(ticket));
                }
                bookedSeats = bookedSeats.substring(1);
              } else {
                List<Seats> availableSeats = getSeats(timeslot);
                for (int i = availableSeats.size() - 1; i >= 0; i--) {
                  if (availableSeats.get(i).getSeatTakenFlag()
                      || availableSeats.get(i).getVipSeatsFlag()) {
                    availableSeats.remove(i);
                  }
                }
                if (availableSeats.size() < totalTickets) {
                  return "Error: Not enough seats available";
                }
                for (int i = 0; i < totalTickets; i++) {
                  String type = "";
                  if (c > 0) {
                    type = "Child";
                    c--;
                  } else if (a > 0) {
                    type = "Adult";
                    a--;
                  } else if (s > 0) {
                    type = "Senior";
                    s--;
                  }
                  int ticket = addTicket(fName, sName, email, username, guest,
                                         type, "card", availableSeats.get(i),
                                         staffId);
                  tickets.add(getTicket(ticket));
                }
              }
              if (!guest) updateCustomerCardDetails(username, card);

              if (!"staff".equalsIgnoreCase(account)) {
                return PdfQrCode.recieptOnlineUser(name, email, card, totalCost,
                                                   tickets, vipSeat,
                                                   bookedSeats);
              } else {
                return PdfQrCode.recieptTillCard(totalCost, card, c, cCost * c,
                                                 a, aCost * a, s, sCost * s,
                                                 bookedSeats, tickets, vipSeat);
              }
            }
          } else {
            return "Error: Incorrect number of seats chosen for VIP.";
          }
        } else {
          return "Error: Invalid slot to book the ticket";
        }
      } else {
        return "Error: Please provide valid details.";
      }
    }
    return "";
  }

  /**
   * Each ticket has to be processed separately
   *
   *
   * @param name
   * @param surname
   * @param email
   * @param username
   * @param guestFlag
   * @param customerType
   * @param paymentType
   * @param seat
   * @param staffId
   * @return
   */
  public int addTicket(String name, String surname, String email,
                       String username, boolean guestFlag, String customerType,
                       String paymentType, Seats seat, String staffId) {

    int seatId = seat.getSeatsId();

    Movies movies = Main.database.getMovieFromSeat(seatId);
    int movieId = movies.getMovieId();
    int customerId = -1;
    Connection connection = data.db.beginTransaction();
    if (guestFlag) {
      customerId = Main.login.createAccountForGuest(name, surname, email);

    } else if (!guestFlag) {
      customerId = getCustomer(username).getCustomersId();
    }
    String query =
                 "UPDATE Seats SET seatTakenFlag=:seatTakenFlag WHERE seatsId=:seatId";
    connection.createQuery(query).addParameter("seatTakenFlag", true)
              .addParameter("seatId", seatId).executeUpdate();
    connection.commit();
    // create the ticket and get id
    Calendar now = Calendar.getInstance();
    Timestamp timestamp = new Timestamp(now.getTimeInMillis());
    query =
          "INSERT INTO Tickets  (timestamp, customerType,paymentType,ticket_seatId) VALUES ( "
            + ":timestamp, :customerType,:paymentType,:ticket_seatId)";
    int ticketId = connection.createQuery(query)
                             .addParameter("timestamp", timestamp)
                             .addParameter("customerType", customerType)
                             .addParameter("paymentType",
                                           paymentType.toLowerCase())
                             .addParameter("ticket_seatId", seatId)
                             .executeUpdate().getKey(Integer.class);
    connection.commit();
    // get one member of the staff and getId
    if (staffId == null) {
      query = "INSERT INTO Customer_Movies(customerMovies_movieId, "
              + "customerMovies_customerId, customerMovies_ticketId) VALUES "
              + "(:customerMovies_movieId, :customerMovies_customerId, "
              + ":customerMovies_ticketId)";
      connection.createQuery(query)
                .addParameter("customerMovies_movieId", movieId)
                .addParameter("customerMovies_customerId", customerId)
                .addParameter("customerMovies_ticketId", ticketId)
                .executeUpdate().getKey(Integer.class);
      connection.commit();
    } else {
      query = "SElECT * FROM Staff WHERE staffId =:staffId";
      List<Staff> staffList = connection.createQuery(query)
                                        .addParameter("staffId", staffId)
                                        .executeAndFetch(Staff.class);
      String staffMember = staffList.get(0).getUsername();
      // add new record in customer movies , movieId,customerId,ticketId,staffId
      query = "INSERT INTO Customer_Movies(customerMovies_movieId, "
              + "customerMovies_customerId, customerMovies_ticketId, "
              + "customerMovies_staffId) VALUES (:customerMovies_movieId, "
              + ":customerMovies_customerId, :customerMovies_ticketId, "
              + ":customerMovies_staffId)";
      connection.createQuery(query)
                .addParameter("customerMovies_movieId", movieId)
                .addParameter("customerMovies_customerId", customerId)
                .addParameter("customerMovies_ticketId", ticketId)
                .addParameter("customerMovies_staffId", staffMember)
                .executeUpdate().getKey(Integer.class);
      connection.commit();
    }
    return ticketId;
  }

  /**
   * Get salt for user from database
   *
   * @param username
   * @return
   */
  public byte[] getSalt(String username) {
    // connect to the database
    try (Connection connection = data.db.open()) {
      // return the image from the database
      String query = "SELECT salt FROM Customers WHERE "
                     + "LOWER(username)=:username";
      List<Customers> cList = connection.createQuery(query)
                                        .addParameter("username",
                                                      username.toLowerCase())
                                        .executeAndFetch(Customers.class);
      if (cList.size() == 1) {
        return cList.get(0).getSalt();
      } else {
        query = "SELECT salt FROM Staff WHERE LOWER(username)=:username";
        List<Staff> sList = connection.createQuery(query)
                                      .addParameter("username",
                                                    username.toLowerCase())
                                      .executeAndFetch(Staff.class);
        if (sList.size() == 1) {
          return sList.get(0).getSalt();
        }
      }
      return null;
    }
  }

  /**
   * Get customer details from database
   *
   * @param username
   * @return
   */
  public Customers getCustomer(String username) {
    // connect to the database
    try (Connection connection = data.db.open()) {
      String query = "SELECT * FROM Customers WHERE LOWER(username)=:username";
      // return the image from the database
      List<Customers> cList = connection.createQuery(query)
                                        .addParameter("username",
                                                      username.toLowerCase())
                                        .executeAndFetch(Customers.class);
      if (cList.size() == 1) {
        return cList.get(0);
      }
      return null;
    }
  }

  /**
   * This checks the database to see if the login is correct for whichever
   * entity is passed
   *
   * @param username the username of user
   * @param password the password of user
   * @return
   */
  public boolean checkCustomerLogin(String username, byte[] password) {
    Customers customer = null;
    if ((customer = getCustomer(username)) != null) {
      if (password.length != customer.getPassword().length) return false;

      for (int i = 0; i < password.length; i++)
        if (password[i] != customer.getPassword()[i]) return false;

      return true;
    }
    return false;
  }

  /**
   * This checks if the customer exists from the username
   *
   * @param username the users login details
   * @return boolean whether true or false
   */
  public boolean customerExistsFromUserName(String username) {
    return getCustomer(username) != null;
  }

  /**
   * Get staff details from database
   *
   * @param username
   * @return
   */
  public Staff getStaff(String username) {
    // connect to the database
    try (Connection connection = data.db.open()) {
      String query = "SELECT * FROM Staff WHERE LOWER(username)=:username";
      // return the image from the database
      List<Staff> sList = connection.createQuery(query)
                                    .addParameter("username",
                                                  username.toLowerCase())
                                    .executeAndFetch(Staff.class);
      if (sList.size() == 1) {
        return sList.get(0);
      }
      return null;
    }
  }

  /**
   * This checks if the staff exists from the login details
   *
   * @param username the login username
   * @return boolean whether it is true or false
   */
  public boolean staffExistsFromUserName(String username) {
    return getStaff(username) != null;
  }

  /**
   * This checks if the staff can login with both username and password
   *
   * @param username staffs login
   * @param password staffs password
   * @return Boolean whether true or false
   */
  public boolean checkStaffLogin(String username, byte[] password) {
    Staff staff = null;
    if ((staff = getStaff(username)) != null) {
      if (password.length != staff.getPassword().length) return false;

      for (int i = 0; i < password.length; i++)
        if (password[i] != staff.getPassword()[i]) return false;

      return true;
    }
    return false;
  }

  /**
   *
   * @param cardNumber
   * @return
   */
  public String anonymizeCard(String cardNumber) {
    if (cardNumber == null) return null;
    return "**** **** **** " + cardNumber.substring(cardNumber.length() - 4);
  }
}
