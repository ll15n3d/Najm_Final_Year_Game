package server.database;

/**
 * This holds details about the staff and allows them to log in to book tickets
 */
public class Staff {

  private int staffId;
  private String username;
  private byte[] password;
  private byte[] salt;

  /**
   * getter for the staffId
   * @return staffId
   */
  public int getStaffId() {
    return staffId;
  }

  /**
   * getter for the username
   * @return username
   */
  public String getUsername() {
    return username;
  }

  /**
   * getter for the password
   * @return password
   */
  public byte[] getPassword() {
    return password;
  }

  /**
   * getter for the salt
   * @return salt
   */
  public byte[] getSalt() {
    return salt;
  }
}
