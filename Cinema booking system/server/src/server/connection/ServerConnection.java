package server.connection;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.plus.webapp.EnvConfiguration;
import org.eclipse.jetty.plus.webapp.PlusConfiguration;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.FragmentConfiguration;
import org.eclipse.jetty.webapp.MetaInfConfiguration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebInfConfiguration;
import org.eclipse.jetty.webapp.WebXmlConfiguration;

import server.Main;
import server.database.Customers;
import server.database.Staff;

/**
 *
 */
public class ServerConnection {

  private Server server;

  /**
   * Runs a server on port 8080.
   * <p>
   * Looks for the jsp files in the folder website/ located in the jar or the
   * folder containing the class files.
   * </p>
   */
  public ServerConnection() {
    final int port = 8080;

    // Create server
    server = new Server();

    // Implement SSL security
    SslContextFactory contextFactory = new SslContextFactory();
    contextFactory.setKeyStorePath(this.getClass().getClassLoader()
                                       .getResource("ssl/keystore")
                                       .toExternalForm());
    contextFactory.setKeyStorePassword("LmAo1237");
    SslConnectionFactory SSLFactory =
                                    new SslConnectionFactory(contextFactory,
                                                             HttpVersion.HTTP_1_1.toString());

    // Setup configuration for ssl
    HttpConfiguration config = new HttpConfiguration();
    config.setSecureScheme("https");
    config.setSecurePort(port);
    HttpConfiguration sslConfiguration = new HttpConfiguration(config);
    sslConfiguration.addCustomizer(new SecureRequestCustomizer());
    HttpConnectionFactory hcf = new HttpConnectionFactory(sslConfiguration);

    // Create connector with SSL security
    ServerConnector connector = new ServerConnector(server, SSLFactory, hcf);
    connector.setPort(port);

    // Connect server to port
    server.setConnectors(new Connector[] { connector });

    // Create handler for the files
    WebAppContext context = new WebAppContext();
    // Set all urls starting with website to use this handler
    context.setContextPath("/website");
    // Set the handler to search in this folder for website files
    String webFiles = this.getClass().getClassLoader().getResource("website/")
                          .toExternalForm();
    context.setResourceBase(webFiles);

    Configuration[] c = new Configuration[] { new AnnotationConfiguration(),
                                              new WebXmlConfiguration(),
                                              new WebInfConfiguration(),
                                              new PlusConfiguration(),
                                              new MetaInfConfiguration(),
                                              new FragmentConfiguration(),
                                              new EnvConfiguration() };
    // Set configurations
    context.setConfigurations(c);

    // Stop it from showing the files in the directory for the website
    context.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed",
                             "false");

    // Servlet handler for data checking and returning json data
    ServletContextHandler context2 = new ServletContextHandler();
    // Set all urls to use this handler
    context2.setContextPath("/");
    context2.addServlet(ServerServlet.class, "/");
    context2.setSessionHandler(new SessionHandler());

    // Set the server handler to both the handlers
    HandlerCollection handlers = new HandlerCollection();
    handlers.setHandlers(new Handler[] { context, context2 });
    server.setHandler(handlers);
  }

  /**
   * Starts the server
   * <p>
   * Prevents other code from running whilst server is running.
   * </p>
   * 
   * @throws Exception
   */
  public void run() throws Exception {
    // Start server
    server.start();
    server.join();
  }

  /**
   * Checks if the user had reached the website with the correct url
   * <p>
   * Also checks if the user has logged in if requireLogin is true
   * </p>
   *
   * @param request Http request from jsp
   * @param response Http response from jsp
   * @param requireLogin True if this page should require a user to be logged in
   *
   * @return True if user has access to the site
   */
  public static boolean check(HttpServletRequest request,
                              HttpServletResponse response,
                              boolean requireLogin) {
    Customers customer = (Customers) request.getSession()
                                            .getAttribute("customer");
    Staff staff = (Staff) request.getSession().getAttribute("staff");
    if (customer != null) {
      request.getSession()
             .setAttribute("customer",
                           Main.database.getCustomer(customer.getUsername()));
    } else if (staff != null) {
      request.getSession()
             .setAttribute("staff",
                           Main.database.getStaff(staff.getUsername()));
    }
    String uri =
               (String) request.getAttribute("javax.servlet.forward.request_uri");
    if (uri.toLowerCase().startsWith("/website")) {
      // Return a page not found error if user tries to access directly
      try {
        response.sendError(HttpServletResponse.SC_NOT_FOUND);
      } catch (IOException e) {
        System.err.println(e);
      }
      return false;
    }

    if (requireLogin) return (Main.login.checkLogin(request));

    return true;
  }

}
