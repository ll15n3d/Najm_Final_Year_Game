package server.connection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import server.Main;
import server.database.Images;
import server.database.Movies;
import server.database.Seats;
import server.database.TimeSlots;

/**
 *
 */
public class ServerServlet extends HttpServlet {

  protected void doGet(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                     IOException {
    try {
      process(request, response);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }

  protected void doPost(HttpServletRequest request,
                        HttpServletResponse response) throws ServletException,
                                                      IOException {
    try {
      process(request, response);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }

  private void process(HttpServletRequest request,
                       HttpServletResponse response) throws ServletException,
                                                     IOException,
                                                     NoSuchAlgorithmException {

    // Fixes bug with session being different every time page is refreshed
    request.getSession();

    // Get the part of the url after the website address
    String url = request.getRequestURI().toLowerCase();

    // If the images folder is requested
    if (url.startsWith("/images/")) {
      // Get image from the database with the filename from the url
      byte[] image = Main.database.getImage(url.substring(8));

      if (image != null) {
        // Set content type to image/format where format is png or jpg etc.
        response.setContentType("image/"
                                + url.substring(url.lastIndexOf(".") + 1));
        response.setContentLength(image.length);

        // Send the image
        response.getOutputStream().write(image);
      } else {
        // Image was not in database
        response.sendError(HttpServletResponse.SC_NOT_FOUND);
      }
      return;
    }
    // If the tickets folder is requested
    if (url.startsWith("/tickets/")) {
      // Get pdf from local folder
      Path pdfPath = Paths.get("." + request.getRequestURI());
      if (pdfPath.toFile().exists()) {
        byte[] ticket = Files.readAllBytes(pdfPath);

        if (ticket != null) {
          // Set content type application/pdf
          response.setContentType("application/pdf");
          response.setContentLength(ticket.length);

          // Send the pdf
          response.getOutputStream().write(ticket);
        } else {
          // Pdf was not in tickets folder
          response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
      }
      return;
    }
    if (url.equals("/login.jsp")) {
      Main.database.data.backgroundTimeSlots(7);
    }

    if (!"json".equalsIgnoreCase(request.getParameter("format"))) {
      // Get handler for jsp files
      ServletContext s = getServletContext().getContext("/website");
      RequestDispatcher r = s.getRequestDispatcher(request.getRequestURI());
      // Forward this to the jsp file handler
      r.forward(request, response);
      return;
    }

    response.setContentType("text/html");
    response.setStatus(HttpServletResponse.SC_OK);

    // JSON data required
    JsonObjectBuilder output = Json.createObjectBuilder();

    if (request.getParameter("signInButton") != null) {
      String errorMessage = Main.login.checkLoginForm(request);
      output.add("status", "".equals(errorMessage) ? "SUCCESS" : "ERROR");
      if (!"".equals(errorMessage)) output.add("message", errorMessage);
    } else if (url.equals("/logout.jsp")) {
      Main.login.logout(request);
      output.add("status", "SUCCESS");
    } else if (url.equals("/whatson.jsp")) {
      output.add("status", "SUCCESS");

      List<Movies> movies = Main.database.queryMovies();
      JsonArrayBuilder moviesArray = Json.createArrayBuilder();

      for (Movies movie : movies) {
        List<Images> images = Main.database.getImages(movie.getMovieId());
        JsonArrayBuilder imagesArray = Json.createArrayBuilder();

        for (Images image : images)
          imagesArray.add(image.getImageId());

        SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
        String duration = f.format(movie.getDuration().getTime());

        List<TimeSlots> slots = Main.database.todaySlots(movie.getMovieId());
        JsonArrayBuilder timeSlots = Json.createArrayBuilder();

        for (TimeSlots slot : slots) {
          int standardSeats = 0;
          JsonArrayBuilder vipSeats = Json.createArrayBuilder();
          List<Seats> seats = Main.database.getSeats(slot.getTimeId());
          for (Seats seat : seats) {
            if (seat.getVipSeatsFlag()) {
              vipSeats.add(seat.getSeatTakenFlag());
            } else if (!seat.getSeatTakenFlag()) {
              standardSeats++;
            }
          }
          timeSlots.add(Json.createObjectBuilder().add("id", slot.getTimeId())
                            .add("start",
                                 f.format(slot.getStartTime().getTime()))
                            .add("screen", slot.getScreen())
                            .add("standard", standardSeats)
                            .add("vip", vipSeats));
        }
        moviesArray.add(Json.createObjectBuilder().add("id", movie.getMovieId())
                            .add("name", movie.getName())
                            .add("blurb", movie.getBlurb())
                            .add("certificate", movie.getCertificate())
                            .add("director", movie.getDirector())
                            .add("lead_actors", movie.getLead_actors())
                            .add("rating", movie.getRating())
                            .add("duration", duration)
                            .add("soldtoday",
                                 Main.database.getSold(movie.getMovieId()))
                            .add("repeats",
                                 Main.database.getRepeats(movie.getMovieId()))
                            .add("slots", timeSlots)
                            .add("images", imagesArray));
      }
      output.add("movies", moviesArray);
    } else if (url.equals("/pay.jsp")) {
      String errorMessage = Main.database.checkTicketForm(request);
      if (errorMessage.startsWith("Error")) {
        output.add("status", "ERROR");
        output.add("message", errorMessage);
      } else {
        output.add("status", "SUCCESS");
        output.add("link", errorMessage);
      }
    }

    // Print JSON data
    response.getWriter().print(output.build());
  }
}
