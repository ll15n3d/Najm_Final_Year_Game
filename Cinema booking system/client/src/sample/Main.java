/*******************************************************************************
 Main class that loads up the initial login.fxml and starts up application.
 ******************************************************************************/
package sample;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.io.IOException;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setResizable(false);
        Scene scene=new Scene(root, 400,450);
        scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        try {
            CommonMethods.connection = new connectionController();
            CommonMethods.server=new Server();
        if (args.length > 0 && args[0].equalsIgnoreCase("remote"))
            CommonMethods.connection.serverUrl = "https://merlin.gotdns.ch/";
        }catch (IOException e){
            e.printStackTrace();
        }

        launch(args);
        String[] name = {"format"};
        String[] value = {"json"};
        CommonMethods.connection.getData("logout.jsp", name, value);
        CommonMethods.running=false;
    }
}
