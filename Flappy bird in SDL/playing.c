#include "playing.h"

bool gameover, hit;
int score=0;
/*---------------------------------------------------------
main loop for playing the game. random walls come in from
the right side to the left and if the bird hits any of the
walls or the ground, game is over.
---------------------------------------------------------*/
void playgame(){
    if(shutdown==true)  return;
    gameover=false;
    hit=false;
    score=0;
    loadpipes();
    resetbirdpos();
    SDL_Event e;
    while(gameover!=true){
        while(SDL_PollEvent(&e)!=0){
            if(e.type==SDL_QUIT){
                gameover=true;
                shutdown=true;
            }
        }
        SDL_RenderClear(render);
        showbackground(true);
        showscore(false);
        showground(true);
        bringpipes();
        if (!gameover) {
            helibird(e);
            SDL_RenderPresent(render);
            frame++;
        }
    }
    frame=0;
}

/*---------------------------------------------------------
renders the bird animation and allows the user to move the
bird up by pressing any key or clicking the mouse, otherwise
the bird slowly falls to the ground.
---------------------------------------------------------*/
SDL_Rect birdpos={40,120,48,28};
double degrees=0;
SDL_RendererFlip flip=SDL_FLIP_NONE;
void helibird(SDL_Event e){
    showbird(false,false);
    if((e.type==SDL_MOUSEBUTTONDOWN || e.type==SDL_KEYDOWN) && birdpos.y>0){
        if(frame%2==0){
            birdpos.y-=1;
            degrees=-10;
        }
    }else if((e.type==SDL_MOUSEBUTTONUP || e.type==SDL_KEYUP ||
          e.type==SDL_MOUSEMOTION) &&birdpos.y<416){
        if(frame%2==0){
            birdpos.y+=1;
            degrees=10;
        }
    }
    SDL_RenderCopyEx(render,birdtexture,NULL,&birdpos,degrees,NULL,flip);
}

SDL_Rect pipe1={290,0,30,113}, pipe2={290,0,30,113};
bool first=true, second=false, start=false; int randomy;
/*---------------------------------------------------------
generates two pipes or walls that come one after the other
from the right side of the window to the left, once one of
them reaches a certain x value, then the next pipe is initiated
---------------------------------------------------------*/
void bringpipes(){
    if(frame==0){
        pipe1.x=290; pipe1.y=0; pipe1.w=30; pipe1.h=113;
        pipe2.x=290; pipe2.y=0; pipe2.w=30; pipe2.h=113;
    }
    generatepipes();
    if(frame%8==0){
        pipe1.x-=2;
        if(start==true){
            pipe2.x-=2;
        }
    }
    checkhit();
    if(pipe1.x==86){
        second=true;
        start=true;
    }else if(pipe2.x==86){
        first=true;
    }
    if(pipe1.x==8 || pipe2.x==8){
        score++;
    }
    SDL_RenderCopy(render,wall1,NULL,&pipe1);
    SDL_RenderCopy(render,wall2,NULL,&pipe2);
}

/*---------------------------------------------------------
     checks if the player hits any wall or ground
---------------------------------------------------------*/
SDL_Rect grnd={0,416,288,96};
void checkhit(){
    if(pipe1.x<=64){
        hit=checkCollision(birdpos, pipe1);
        if(hit==true) {
            gameover=true;
        }
    }else if(pipe2.x<=64){
        hit=checkCollision(birdpos, pipe2);
        if(hit==true) {
            gameover=true;
        }
    }
    hit=checkCollision(birdpos, grnd);
    if(hit==true) {
        gameover=true;
    }
}

/*---------------------------------------------------------
generates random new walls by simply decreasing the x value
of one wall until its less than 0 where it will no longer be
visible, then sets the x back to 290 which is more than the
screen size 288 making it look like a continous animation.
---------------------------------------------------------*/
void generatepipes(){
    srand(time(NULL));
    if (first==true){
        pipe1.x=290;
        randomy=(int)rand()%303+1;
        pipe1.y=randomy;
        first=false;
    }else if(second==true){
        pipe2.x=290;
        randomy=(int)rand()%303+1;
        pipe2.y=randomy;
        second=false;
    }
}

/*---------------------------------------------------------
loads relevant graphics for this page into textures aswell
as loading a font for displaying the score on screen.
---------------------------------------------------------*/
SDL_Texture* wall1=NULL;
SDL_Texture* wall2=NULL;
TTF_Font* font=NULL;
void loadpipes(){
    temp=IMG_Load("graphics/wall.png");
    wall1=SDL_CreateTextureFromSurface(render,temp);
    wall2=SDL_CreateTextureFromSurface(render,temp);
    font=TTF_OpenFont("fonts/OpenSans-Regular.ttf",45);
}

/*---------------------------------------------------------
resets the birdpos back to its default place rather than the
floor which would mean gameover.this is needed if the user
wants to play again at which point prior to the use of this
function, the bird would have been on the floor.
---------------------------------------------------------*/
void resetbirdpos(){
    birdpos.x=40;
    birdpos.y=120;
    birdpos.w=48;
    birdpos.h=28;
}

/*---------------------------------------------------------
displays the score on the screen by converting the score into
an array of chars which can then be printed on screen.
---------------------------------------------------------*/
SDL_Texture* displayscore=NULL;
SDL_Color textcolor={255,255,255};
SDL_Rect corner={118,10,51,50};
char numbers[5];
void showscore(bool moveposition){
    if(frame==0){
        corner.y=10;
        corner.x=118;
    }
    sprintf(numbers,"%d",score/8);

    temp=TTF_RenderText_Solid(font,numbers,textcolor);
    displayscore=SDL_CreateTextureFromSurface(render,temp);
    if(moveposition==true){
        corner.y=180;
        corner.x=160;
    }
    SDL_RenderCopy(render,displayscore,NULL,&corner);
}
