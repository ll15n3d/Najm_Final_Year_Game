#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <stdbool.h>
#include <stdlib.h>

//global variables used throughout
extern SDL_Window* Window;
extern SDL_Renderer* render;
extern int frame;
extern SDL_Surface* temp;

void init();
bool checkCollision(SDL_Rect a, SDL_Rect b);
void closesdl();
