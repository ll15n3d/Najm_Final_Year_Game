#include "algorithms.h"
//functions prototypes
double time_insertionsort(int *array, int length);
double time_bubbleSort(int *array, int length);
double time_selectionsort(int *array, int length);
double time_quicksort(int *array, int start, int end);
double time_mergesort(int *array, int length);
void testall(int *array, int length);
void fill(int *original, int *array, int length);
