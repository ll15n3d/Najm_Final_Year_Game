#include "result.h"

SDL_Texture* oversign=NULL;
SDL_Texture* replaybtn=NULL;
SDL_Texture* exitbtn=NULL;
SDL_Texture* medal=NULL;
SDL_Texture* silver=NULL;
SDL_Texture* gold=NULL;
/*---------------------------------------------------------
another event loop that continously renders to the screen
in order to show the result of the game. the user can see
their score, see which medal they got depending on their
score, and what they want to do next.
---------------------------------------------------------*/
bool reply, loop;
bool showresults(){
    if(shutdown==true)  return false;
    reply=true;
    loop=false;
    loadresultmedia();
    SDL_Event e;

    while(loop!=true){
        while(SDL_PollEvent(&e)!=0){
            if(e.type==SDL_QUIT){
                loop=true;
                shutdown=true;
            }
        }
        SDL_RenderClear(render);
        showbackground(false);
        showground(false);
        stopbackground();
        showsign_btn();
        showmedal();
        handlebutton(e);
        handleexitbtn(e);
        SDL_RenderPresent(render);
        frame++;
    }
    frame=0;
    return reply;
}

/*---------------------------------------------------------
shows the ground and the background as they were just before
gameover and if the user hits one of the walls, then its y
value is slowly reduced until it hits the ground. while the
bird is falling, the texture is rotated to create animation
---------------------------------------------------------*/
double degre=0; SDL_RendererFlip flp=SDL_FLIP_NONE;
void stopbackground(){
    showbird(false,false);
    if(birdpos.y<416){
        birdpos.y+=3;
        degre-=15;
    }
    SDL_RenderCopyEx(render,birdtexture,NULL,&birdpos,degre,NULL,flp);
    SDL_RenderCopy(render,wall1,NULL,&pipe1);
    SDL_RenderCopy(render,wall2,NULL,&pipe2);
}

/*---------------------------------------------------------
    loads relevant graphics for this page into textures
---------------------------------------------------------*/
void loadresultmedia(){
    temp=IMG_Load("graphics/game-over.png");
    oversign=SDL_CreateTextureFromSurface(render, temp);
    temp=IMG_Load("graphics/restart.png");
    replaybtn=SDL_CreateTextureFromSurface(render, temp);

    temp=IMG_Load("graphics/bronze.png");
    medal=SDL_CreateTextureFromSurface(render, temp);
    temp=IMG_Load("graphics/silver.png");
    silver=SDL_CreateTextureFromSurface(render, temp);

    temp=IMG_Load("graphics/gold.png");
    gold=SDL_CreateTextureFromSurface(render, temp);
    temp=IMG_Load("graphics/exitb.png");
    exitbtn=SDL_CreateTextureFromSurface(render, temp);
}

/*---------------------------------------------------------
reveals the gameover sign and buttons by slowly increasing
the opacity from 0 to 255.
---------------------------------------------------------*/
SDL_Rect over={44,60,200,50};
SDL_Rect playagain_btn={20,340,114,65};
SDL_Rect exit_btn={154,340,114,65};
int a=50;
bool moveon=false;
void showsign_btn(){
    if(frame==0){
        a=50;
        moveon=false;
    }
    if(a!=255){
        a+=5;
        SDL_SetTextureAlphaMod(oversign,a);
        SDL_SetTextureAlphaMod(replaybtn,a);
        SDL_SetTextureAlphaMod(exitbtn,a);
    }else{
        moveon=true;
    }
    SDL_RenderCopy(render,oversign,NULL,&over);
    if(moveon==false){
        SDL_RenderCopy(render,replaybtn,NULL,&playagain_btn);
        SDL_RenderCopy(render,exitbtn,NULL,&exit_btn);
    }
}

/*---------------------------------------------------------
handles events on the restart button, hovering over will
change the colour of the button which is simply done by
loading a new image into the texture. clicking on the button
will restart the game.
---------------------------------------------------------*/
void handlebutton(SDL_Event event){
    int x, y;
    temp=IMG_Load("graphics/restart.png");
    SDL_GetMouseState(&x,&y);
    if(x>20 && x<134 && y>340 && y<405){
        if(event.type==SDL_MOUSEMOTION){
            temp=IMG_Load("graphics/restart_down.png");
        }else if(event.type==SDL_MOUSEBUTTONDOWN){
            loop=true;
        }
    }
    replaybtn=SDL_CreateTextureFromSurface(render, temp);
    if(moveon==true){
        SDL_RenderCopy(render,replaybtn,NULL,&playagain_btn);
    }
}

/*---------------------------------------------------------
handles events on the exit button, hovering over will
change the colour of the button which is simply done by
loading a new image into the texture. clicking on the button
will exit the game and close the window.
---------------------------------------------------------*/
void handleexitbtn(SDL_Event event){
    int x, y;
    temp=IMG_Load("graphics/exitb.png");
    SDL_GetMouseState(&x,&y);
    if(x>154 && x<268 && y>340 && y<405){
        if(event.type==SDL_MOUSEMOTION){
            temp=IMG_Load("graphics/exitb_down.png");
        }else if(event.type==SDL_MOUSEBUTTONDOWN){
            shutdown=true;
            loop=true;
            reply=false;
        }
    }
    exitbtn=SDL_CreateTextureFromSurface(render, temp);
    if(moveon==true){
        SDL_RenderCopy(render,exitbtn,NULL,&exit_btn);
    }
}

/*---------------------------------------------------------
displays the medal and score made on the screen. if score is
less than 10 then bronze is shown, if between 10 and 20, then
reward is silver medal. anything higher will show a gold medal.
---------------------------------------------------------*/
SDL_Rect medal_Screen={25,512,238,122};
int i=5;
void showmedal(){
    if(frame==0){
        i=5;
        medal_Screen.y=512;
    }
    if(a>=255){
        if(medal_Screen.y>150){
            medal_Screen.y-=10;
            if(i!=255){
                i+=10;
                SDL_SetTextureAlphaMod(medal,i);
            }
        }
    }
    if(score/8<10){
        SDL_RenderCopy(render, medal,NULL, &medal_Screen);
    }else if(score/8>=10 && score/8<20){
        SDL_RenderCopy(render, silver,NULL, &medal_Screen);
    }else{
        SDL_RenderCopy(render, gold,NULL, &medal_Screen);
    }
    if(a==255 && i==255){
        showscore(true);
    }
}

/*---------------------------------------------------------
close and frees memory used by sdl texturs, this can be done
since most of the textures are global. if the user wants to
play the game again, then only some textures are destroyed
as they will be recreated again.
---------------------------------------------------------*/
void leave(){
        SDL_DestroyTexture(instructions);
        SDL_DestroyTexture(readysign);
        TTF_CloseFont(font);
        SDL_DestroyTexture(wall1);
        SDL_DestroyTexture(wall2);
        SDL_DestroyTexture(displayscore);
        SDL_DestroyTexture(oversign);
        SDL_DestroyTexture(replaybtn);
        SDL_DestroyTexture(exitbtn);
        SDL_DestroyTexture(medal);
        SDL_DestroyTexture(silver);
        SDL_DestroyTexture(gold);
        SDL_FreeSurface(temp);
        SDL_DestroyTexture(ground);
        SDL_DestroyTexture(background);
        SDL_DestroyTexture(birdtexture);
        SDL_DestroyTexture(title);
        SDL_DestroyTexture(playbtn);
        SDL_DestroyTexture(sortbtn);
        SDL_DestroyRenderer(render);
        SDL_DestroyWindow(Window);
}
