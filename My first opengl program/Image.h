#ifndef _IMAGE_
#define _IMAGE_
#include<string>
#include <QImage>
#include <GL/glu.h>

/*-----------------------------------------------------------------------------
    Header file and class declaration for Image.cpp which has been obtained
    from one of the tutorials. Required slight modification so that it is now
    able to flip the image upside down if required.
-----------------------------------------------------------------------------*/
class Image {
    public:
        Image(const std::string& fn, bool flipvertical);
        ~Image();
        const GLubyte* imageField() const;
        unsigned int Width()  const { return _width;}
        unsigned int Height() const {return _height;}

    private:
        Image(const Image&);
        unsigned int _width;
        unsigned int _height;
        QImage* p_qimage;
        GLubyte* _image;
};
#endif
