#include <QGLWidget>
#include <GL/glut.h>
#include <math.h>
#include <QtGui>

/*-----------------------------------------------------------------------------
    Header file and class declaration for Snow.cpp. This class, similar to the
    Gem class, renders a single small white sphere at the top of the orthographic
    volume using the draw function. The move function is used to slowely decrement
    the y value which results in a  falling snow effect.
-----------------------------------------------------------------------------*/
class Snow{
    public:
        Snow(float x1, float y1, float z1);
        void move();
        void draw();

        float x, y, z;
        bool falling=true;
};
