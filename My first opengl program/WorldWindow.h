#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1
#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QFont>
#include "WorldWidget.h"

/*-----------------------------------------------------------------------------
    Header file and class declaration for WorldWindow.cpp. This class simply
    creates a QBoxLayout and adds to its centre an instance of WorldWidget. The
    layout also includes some sliders at the bottom to control certain features
    of the program especially the animations.
-----------------------------------------------------------------------------*/
class WorldWindow: public QWidget{
	public:
    	WorldWindow(QWidget *parent); // constructor
    	~WorldWindow();    // destructor
        void ResetInterface(); // resets all the interface elements

    	QBoxLayout *windowLayout; 	// window layout
    	WorldWidget *myWidget; // beneath that, the main widget
    	QSlider *rotateview; // and a slider for rotating the view
        QSlider *personspeed; // and a slider for the speed at which the person moves
        QSlider *cameraspeed; // and a slider for the mouse sensitivity
        QSlider *snowamount; // and a slider for the amount of snow that falls
};
#endif
