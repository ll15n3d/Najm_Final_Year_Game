#include "WorldWindow.h"

/*-----------------------------------------------------------------------------
    Class Constructor, creates a QBoxLayout and adds a WorldWidget in its centre
    and also some sliders for user interaction at the bottom of the layout.
-----------------------------------------------------------------------------*/
WorldWindow::WorldWindow(QWidget *parent): QWidget(parent){
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this); // create the window layout
    QVBoxLayout *layout= new QVBoxLayout();

    //remove empty spaces from window
    windowLayout->setMargin(0);
    windowLayout->setContentsMargins(QMargins(0,0,0,0));
    windowLayout->setSpacing(0);
    layout->setAlignment(Qt::AlignBottom);
    windowLayout->setAlignment(Qt::AlignCenter);

    // create main widget
	myWidget = new WorldWidget(this);
    myWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	windowLayout->addWidget(myWidget);

    // create rotating camera slider       Row 1
    QFont newFont("Arial",10);
	rotateview = new QSlider(Qt::Horizontal);
    rotateview->setRange(0,360);
    connect(rotateview, SIGNAL(valueChanged(int)), myWidget, SLOT(sliderUpdate(int)));
    QLabel *label1 = new QLabel("Rotate View", this);
    label1->setFont(newFont);
    layout->addWidget(label1);
	layout->addWidget(rotateview);

    // create animation and camera speed sliders        Row 2
    QVBoxLayout *layout2=new QVBoxLayout();
	personspeed = new QSlider(Qt::Horizontal);
    personspeed->setRange(0,100);
    personspeed->setValue(50);
    connect(personspeed, SIGNAL(valueChanged(int)), myWidget, SLOT(changePersonSpeed(int)));
    QLabel *label2 = new QLabel("Ghost Speed", this);
    label2->setFont(newFont);
    layout2->addWidget(label2);
	layout2->addWidget(personspeed);

    QVBoxLayout *layout3=new QVBoxLayout();
    cameraspeed = new QSlider(Qt::Horizontal);
    cameraspeed->setRange(0,100);
    cameraspeed->setValue(20);
    connect(cameraspeed, SIGNAL(valueChanged(int)), myWidget, SLOT(changeMouseSensitivty(int)));
    QLabel *label3 = new QLabel("Mouse Sensitivity", this);
    label3->setFont(newFont);
    layout3->addWidget(label3);
	layout3->addWidget(cameraspeed);

    QHBoxLayout *second_row=new QHBoxLayout();
    second_row->addLayout(layout2);
    second_row->addLayout(layout3);
    layout->addLayout(second_row);

    // create snow slider      Row 3
    snowamount = new QSlider(Qt::Horizontal);
    snowamount->setRange(0,100);
    snowamount->setValue(50);
    connect(snowamount, SIGNAL(valueChanged(int)), myWidget, SLOT(changeSnowAmount(int)));
    QLabel *label4 = new QLabel("Snow Amount", this);
    label4->setFont(newFont);
    layout->addWidget(label4);
    layout->addWidget(snowamount);

    //finally
    windowLayout->addLayout(layout);
}

/*-----------------------------------------------------------------------------
                                Class destructor.
-----------------------------------------------------------------------------*/
WorldWindow::~WorldWindow(){
	delete myWidget;
	delete windowLayout;
}

/*-----------------------------------------------------------------------------
                    resets all the interface elements
-----------------------------------------------------------------------------*/
void WorldWindow::ResetInterface(){
	rotateview->setMinimum(0);
	rotateview->setMaximum(360);

	// now force refresh
	myWidget->update();
	update();
}
