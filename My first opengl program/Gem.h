#include <QGLWidget>
#include <GL/glut.h>
#include <math.h>
#include <QtGui>

/*-----------------------------------------------------------------------------
Header File and class declaration for Gem.cpp. Declares all relevant fields
and functions that are required. This class is essentially a octahedron that
is made from GL Triangles, the shape is scaled down to make it small and the
functions display a small animation where the shape looks like a gem and falls
from the top of the orthographic volume and also rotates while falling.
-----------------------------------------------------------------------------*/
class Gem{
    public:
        Gem(float x1, float y1, float z1);
        void move();
        void draw();
        void displayConvex();
        void drawPyramid();

        float x, y, z;
        int angle=0;
        bool falling=true;
};
